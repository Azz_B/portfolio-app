import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  height: 700px;
  margin-top: 79px;
  background-color: #424242;
`

const AboutMe = styled.div`
  border: 1px solid #eaeaea;
  height: 400px;
  max-width: 1000px;
  margin: 100px auto;
  padding: 20px;
  background-color: white;
  font-size: 1.5em;
`

class Header extends React.Component {
  render() {
    return (
      <Wrapper>
        <AboutMe>{this.props.intro}</AboutMe>
      </Wrapper>
    )
  }
}

Header.propTypes = {
  intro: PropTypes.string,
}

export default Header
