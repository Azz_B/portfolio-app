import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  min-height: 500px;
  max-width: 1000px;
  margin: 30px auto;
  position: relative;
  background-color: white;
`

class AnonymouseMessage extends React.Component {
  render() {
    return (
      <Wrapper>

      </Wrapper>
    )
  }
}

export default AnonymouseMessage
