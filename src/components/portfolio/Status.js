import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  min-height: 200px;
  max-width: 800px;
  margin: 0 auto;
  position: relative;
  top: -50px;
  z-index: 99;
  background-color: white;
  padding: 20px;
  font-size: 1.5em;
`

class Status extends React.Component {
  render() {
    return (
      <Wrapper>
        {this.props.content}
      </Wrapper>
    )
  }
}

Status.propTypes = {
  content: PropTypes.string,
}

export default Status
