import React from 'react'
import styled from 'styled-components'
import UserMenu from '../__global__/UserMenu'
import logo from './logo.png'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  padding: 0 20px;
  height: 80px;
  position: fixed;
  top: 0;
  width: 100%;
  background-color: white;
`

const Logo = styled.div`
  float: left;
  height: 75px;
  width: 150px;
  margin-top: 2px;
  background-image: url(${logo});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
`

const LoginLink = styled.div`
  float: right;
  border: 3px solid #fe6b6b;
  border-radius: 30px;
  color: white;
  font-size: 1.1em;
  font-weight: bold;
  background-color: #fe6b6b;
  width: 150px;
  height: 50px;
  margin-top: 15px;
  line-height: 50px;
  text-align: center;

  &:hover {
    cursor: pointer;
    opacity: 0.8;
  }
`

const BlogsLink = styled.div`
  float: right;
  width: 150px;
  height: 50px;
  margin-top: 15px;
  line-height: 56px;
  text-align: center;

  &:hover {
    cursor: pointer;
    text-decoration: underline;
    opacity: 0.8;
  }
`

class Navbar extends React.Component {
  render() {
    const { history, auth } = this.props
    return (
      <Wrapper>
        <Logo />
        {auth.username? <UserMenu size={60} history={history} auth={auth}/> : <LoginLink onClick={() => history.push('/login')}>LOGIN</LoginLink>}
        <BlogsLink onClick={() => history.push('/feeds')}>Blogs</BlogsLink>
      </Wrapper>
    )
  }
}

export default Navbar
