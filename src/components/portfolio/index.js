import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { mainPortfolio } from '../../store/actions/portfolio'
import { loggedUserSelector } from '../../store/selectors/auth'
import { mainPortfolioSelector } from '../../store/selectors/portfolios'

class Container extends React.Component {

  componentDidMount() {
    this.props.mainPortfolioRequest()
  }

  render() {
    return <Component {...this.props}/>
  }
}

const mapStateToProps = (state) => ({
  auth: loggedUserSelector(state),
  portfolio: mainPortfolioSelector(state),
})

const mapDispatchToProps = (dispatch) => bindActionCreators({mainPortfolioRequest: mainPortfolio.request}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Container)
