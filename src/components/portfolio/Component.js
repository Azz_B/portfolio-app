import React from 'react'
import styled from 'styled-components'
import Navbar from './Navbar'
import Header from './Header'
import Status from './Status'
import AnonymouseMessage from './AnonymouseMessage'

const Wrapper = styled.div`
  background-color: #fcfcfc;
`

class Portfolio extends React.Component {

  render() {
    const { intro, status, auth, portfolio, ...rest } = this.props
    return (
      <Wrapper>
        <Navbar {...rest} auth={auth}/>
        <Header intro={portfolio.intro}/>
        <Status content={portfolio.status}/>
        <AnonymouseMessage />
      </Wrapper>
    )
  }
}

export default Portfolio
