import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  height: 400px;
  margin: 30px 0;
  background-color: white;
`
const Header = styled.div`
  height: 80%;
  cursor: pointer;
`
const Info = styled.div`
  height: 20%;
  padding: 0 20px;
`
const UserCreator = styled.img`
  border: 1px solid black;
  border-radius: 50%;
  height: 60px;
  width: 60px;
  margin-top: 10px;
  display: inline-block;
  float: left;
  cursor: pointer;
`
const Username = styled.p`
  float: left;
  display: inline-block;
  line-height: 80px;
  margin-left: 20px;
  font-size: 16px;

`
const Title = styled.p`
  margin: 0 20px;
  font-size: 2em;
  text-align: center;
  line-height: 320px;
`

class Post extends React.Component {
  render() {
    const { post, history } = this.props

    return (
      <Wrapper>
        <Header onClick={() => history.push(`/post/${post.id}`)}>
          <Title>{post.title}</Title>
        </Header>
        <Info>
          <UserCreator src={post.user.photo} onClick={() => history.push(`/profile/${post.user.username}`)}/>
          <Username>{post.user.fullname}</Username>
        </Info>
      </Wrapper>
    )
  }
}


export default Post
