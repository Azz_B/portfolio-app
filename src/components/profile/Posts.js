import React from 'react'
import styled from 'styled-components'
import Post from './Post'
import InfiniteScroll from '../__global__/InfiniteScroll'

const Wrapper = styled.div`
  max-width: 1020px;
  margin: 90px auto;
  min-height: 600px;
`
const NoPostsWrapper = styled.div`
  border: '1px solid #eaeaea'
  backgroundColor: white;
  max-width: 1020px;
  margin: 90px auto;
  min-height: 600px;
  padding: 50px;
  text-align: center;
`

function NoPosts() {
  return (
    <NoPostsWrapper >
      <h1 style={{color: '#cccccc', fontSize: '3em'}}>No Posts</h1>
    </NoPostsWrapper>
  )
}

class Posts extends React.Component {

  render() {
    const { posts, ...rest } = this.props
    if(posts.length === 0) return <NoPosts />

    return (
      <Wrapper>
        {posts.map(post => <Post {...rest} key={post.id} post={post}/>)}
      </Wrapper>
    )
  }
}


export default InfiniteScroll(Posts)
