import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  height: 400px;
  max-width: 1020px;
  margin: 20px auto;
  background-color: white;
  padding: 50px 0px;
  text-align: center;
`

class ProfileNotFound extends React.Component {
  render() {
    return (
      <Wrapper>
        <h1 style={{color: '#cccccc', fontSize: '3em'}}>Profile Not Found</h1>
      </Wrapper>
    )
  }
}

export default ProfileNotFound
