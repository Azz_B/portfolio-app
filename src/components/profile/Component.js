import React from 'react'
import styled from 'styled-components'
import Navbar from './Navbar'
import ProfileInfo from './ProfileInfo'
import Posts from './Posts'

const Wrapper = styled.div`
  background-color: #fcfcfc;
`

class Component extends React.Component {
  render() {
    const { auth, profile, posts, loadPosts, ...rest } = this.props
    
    return (
      <Wrapper>
        <Navbar {...rest} auth={auth} />
        <ProfileInfo {...rest} profile={profile} />
        <Posts {...rest} posts={posts} load={() => loadPosts()} loading={this.props.ui.spinners.posts}/>
      </Wrapper>
    )
  }
}

export default Component
