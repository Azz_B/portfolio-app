import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { get_user } from '../../store/actions/users'
import { get_user_posts } from '../../store/actions/posts'
import { profileSelector, profilePostsSelector } from '../../store/selectors/users'
import { loggedUserSelector } from '../../store/selectors/auth'
import { uiSelector } from '../../store/selectors/ui'

const screen = 'profile'

class Container extends React.Component {

  componentDidMount() {
    this.username = this.props.match.params.username
    this.props.getUserRequest({username: this.username, meta: {screen, element: 'userInfo'}})
    this.loadPosts()
  }

  loadPosts = () => {
    this.props.getUserPostsRequest({username: this.username, meta: {screen, element: 'posts'}})
  }

  render() {
    return <Component {...this.props} loadPosts={this.loadPosts}/>
  }
}

const mapStateToProps = (state, props) => ({
  auth: loggedUserSelector(state),
  profile: profileSelector(state, { username: props.match.params.username }),
  posts: profilePostsSelector(state, { username: props.match.params.username }),
  ui: uiSelector(state, {screen})
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getUserRequest: get_user.request,
  getUserPostsRequest: get_user_posts.request,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Container)
