import React from 'react'
import styled from 'styled-components'
import { Loader, Segment } from 'semantic-ui-react'
import ProfileNotFound from './ProfileNotFound'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  height: 400px;
  max-width: 1020px;
  margin: 20px auto;
  margin-top: 110px;
  background-color: white;
`

const Box = styled.div`
  width: ${(props) => props.width+'%'};
  height: 400px;
  float: left;
`

const Photo = styled.img`
  border: 1px solid black;
  width: 200px;
  height: 200px;
  border-radius: 50%;
  margin: 20px auto;


  @media screen and (max-width: 750px) {
    width: 120px;
    height: 120px;
  }
`

const FullName = styled.h3`
  font-size: 26px;
`

const Bio = styled.p`
  font-size: 19px;
  margin-top: 40px;
`

const Follow = styled.p`
  border: 1px solid #00b3c6;
  border-radius: 5%;
  color: ${({on}) => on? 'white' : '#00b3c6'};
  background-color: ${({on}) => !on? 'white' : '#00b3c6'};
  font-size: 19px;
  font-weight: bold;
  max-width: 200px;
  height: 50px;
  margin: 30px auto;
  text-align: center;
  line-height: 50px;

  &:hover {
    cursor: pointer;
    opacity: 0.9;
  }
`

class ProfileInfo extends React.Component {
  render() {
    if(this.props.ui.spinners.userInfo) return <Wrapper><Segment style={{height: '400px'}}><Loader active >Loading profile info</Loader></Segment></Wrapper>
    if(!this.props.profile.username) return <ProfileNotFound />

    const { fullname, bio, photo, you_follow } = this.props.profile

    return (
      <Wrapper>
        <Box width={70} style={{padding: 30}}>
          <FullName>{fullname}</FullName>
          <Bio>{bio}</Bio>
        </Box>
        <Box width={30}>
          <Photo src={photo}/>
          <Follow on={you_follow}> {you_follow? 'Following' : 'Follow'} </Follow>
        </Box>
      </Wrapper>
    )
  }
}

export default ProfileInfo
