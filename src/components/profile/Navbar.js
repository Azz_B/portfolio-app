import React from 'react'
import styled from 'styled-components'
import Dropdown from '../__global__/Dropdown'
import UserMenu from '../__global__/UserMenu'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  height: 90px;
  background-color: white;
  padding: 0 20px;
  position: fixed;
  top: 0;
  width: 100%;
`

const LeftItems = styled.div`
  display: inline-block;
  float: right;
`

const LoginLink = styled.div`
  float: right;
  border: 1px solid #fe6b6b;
  width: 150px;
  height: 50px;
  margin-top: 15px;
  line-height: 55px;
  text-align: center;
  font-size: 1.2em;
  font-weight: bold;
  color: white;
  background-color: #fe6b6b;

  &:hover {
    cursor: pointer;
    opacity: 0.9;
  }
`

class Navbar extends React.Component {

  render() {
    return (
      <Wrapper>
        {this.props.auth.username? <UserMenu {...this.props}/> : <LoginLink onClick={() => this.props.history.push('/login')}>LOGIN</LoginLink>}
      </Wrapper>
    )
  }
}

export default Navbar
