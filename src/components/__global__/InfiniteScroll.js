import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'


const Loading = styled.div`
  height: 80px;
  text-align: center;
  line-height: 80px;
  font-size: 2em;
  color: #d8d8d8;
  display: ${({loading}) => loading? 'block' : 'none'}
`


function InfiniteScroll(Component) {

  return class  extends React.Component {
    componentDidMount() {
      this.last_scrollY = window.scrollY
      this.element = ReactDOM.findDOMNode(this)
      window.addEventListener('scroll', this.onScroll, false)
    }

    componentWillUnmount() {
      window.removeEventListener('scroll', this.onScroll, false)
    }

    onScroll = () => {
      const is_down = this.last_scrollY < window.scrollY
      this.last_scrollY = window.scrollY
      if((window.innerHeight + window.scrollY) >= (this.element.offsetHeight - 900) && is_down) {
        this.props.load()
      }
    }

    render() {
      return (
        <div>
          <Component {...this.props}/>
          <Loading loading={this.props.loading}>Loading feeds....</Loading>
        </div>
      )
    }
  }

}

export default InfiniteScroll
