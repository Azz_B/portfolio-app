import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { logout } from '../../store/actions/auth'
import Dropdown from './Dropdown'

function UserMenu({logoutRequest, history, auth, size}) {
  const item_style = {lineHeight: '50px', fontSize: '17px', paddingLeft: '5px'}

  return (
    <Dropdown style={{float: 'right', marginTop: '12px'}} size={size} img={`http://localhost:4000/api_v1/users/${auth.username}/photo`}>
      <Dropdown.Item style={item_style} onClick={() => history.push(`/profile/${auth.username}`)}>Profile</Dropdown.Item>
      <Dropdown.Item style={item_style} onClick={() => history.push('/settings')}>Settings</Dropdown.Item>
      <Dropdown.Item style={item_style}>Dashboard</Dropdown.Item>
      <Dropdown.Item style={item_style} onClick={() => history.push('/new-post')}>New Post</Dropdown.Item>
      <Dropdown.Item style={item_style} onClick={() => logoutRequest()}>Logout</Dropdown.Item>
    </Dropdown>
  )
}

const mapStateToProps = () => ({})
const mapDispatchToProps = dispatch => bindActionCreators({
  logoutRequest: logout.request,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(UserMenu)
