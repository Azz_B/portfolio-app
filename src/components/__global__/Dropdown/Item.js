import React from 'react'
import PropTypes from 'prop-types';
import styled from 'styled-components'

const Wrapper = styled.div`
  height: 50px;
  padding: 3px;

  &:hover {
    cursor: pointer;
    background-color: #f2f2f2;
  }
`

class Item extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { onClick, key, ...rest } = this.props

    return (
      <Wrapper {...rest} onClick={() => this.context.onItemClick(key, onClick)}>
        {this.props.children}
      </Wrapper>

    )
  }
}

Item.contextTypes = {
  onItemClick: PropTypes.func,
}

export default Item
