import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types';
import styled from 'styled-components'
import Item from './Item'

const Wrapper = styled.div`
  position: relative;
`
const Circle = styled.div`
  border: 1px solid #cccccc;
  height: ${({size}) => size? size+'px' : '70px'};
  width: ${({size}) => size? size+'px' : '70px'};
  border-radius: 50%;
  box-shadow: ${({open}) => open? '0 2px 3px rgba(0,0,0,0.16), 0 2px 3px rgba(0,0,0,0.23)' : 'none'};

  background-image: ${({img}) => img? `url(${img})` : 'none'};
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;

  &:hover {
    cursor: pointer;
  }
`

const Arrow = styled.div`
  position: relative;
  top: -7px;
  right: -110px;
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-bottom: 5px solid black;
`

const Menu = styled.div`
  border: 1px solid #cccccc;
  background-color: white;
  position: absolute;
  right: 0px;
  min-width: 150px;
  margin-top: 10px;
  box-shadow: 0 2px 3px rgba(0,0,0,0.16), 0 2px 3px rgba(0,0,0,0.23);
  transition: display 2s;
  display: ${({open}) => open? 'block' : 'none'};
`

class DropDown extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
    }
  }

  getChildContext() {
    return {
      onItemClick: (key, onClick) => {
        this.setState({open: false})
        onClick && onClick(key)
      },
    }
  }

  componentDidMount() {
    document.addEventListener('click', this.clickOutSideEvent.bind(this))
  }

  // componentDidUpdate() {
  //   this.lastWindowClickEvent = this.clickOutSideEvent.bind(this)
  //   document.addEventListener('click', this.lastWindowClickEvent)
  //   this.lastWindowClickEvent = null
  // }

  componentWillUnmount() {
    this.clickOutSideEvent = () => {console.log('nothing');}
    document.removeEventListener('click', this.clickOutSideEvent.bind(this));
  }

  clickOutSideEvent(e) {
    // TODO: I should remove try catch and fix the error
    if(!this.state.open) return ;
    try {
      const node = ReactDOM.findDOMNode(this)
      let target = e.target

      while(target.parentNode) {
        if(node === target) return;
        target = target.parentNode
      }

      this.close()
    } catch (e) {
      //console.log(e);
    }
  }

  close(e) {
    if(this.state.open) this.setState({open: false})
  }

  render() {
    const { onClick, children, img, open, size, ...rest } = this.props

    return (
      <Wrapper {...rest}>
        <Circle size={size} img={img} open={this.state.open} onClick={() => this.setState({open: !this.state.open})} />
        <Menu open={this.state.open}>
          <Arrow />
          {children}
        </Menu>
      </Wrapper>

    )
  }
}

DropDown.childContextTypes = {
  onItemClick: PropTypes.func,
}

DropDown.Item = Item

export default DropDown
