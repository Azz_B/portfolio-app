import React from 'react'
import styled from 'styled-components'
import Navbar from './Navbar'
import Editor from './Editor'

const Wrapper = styled.div`
  background-color: #fcfcfc;
`

class Draft extends React.Component {
  render() {
    const { auth, draft, uploadDraftValuesRequest, publishPostRequest, ...rest } = this.props
    return (
      <Wrapper>
        <Navbar {...rest} auth={auth} draft_id={draft.id} publishPost={publishPostRequest}/>
        <Editor {...rest} draft={draft} uploadDraftValuesRequest={uploadDraftValuesRequest} />
      </Wrapper>
    )
  }
}

export default Draft
