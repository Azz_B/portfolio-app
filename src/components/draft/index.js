import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { uploadDraftValues, publishPost } from '../../store/actions/posts'
import { selectedDraftSelector } from '../../store/selectors/posts'
import { loggedUserSelector } from '../../store/selectors/auth'
import { uiSelector } from '../../store/selectors/ui'

const screen = 'draft'

class Container extends React.Component {

  componentDidMount() {

  }

  render() {
    console.log('my props: ', this.props);
    return <Component {...this.props}/>
  }
}

const mapStateToProps = (state, props) => ({
  auth: loggedUserSelector(state),
  ui: uiSelector(state, {screen}),
  draft: selectedDraftSelector(state),
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  uploadDraftValuesRequest: uploadDraftValues.request,
  publishPostRequest: publishPost.request,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Container)
