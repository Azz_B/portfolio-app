import React from 'react'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import styled from 'styled-components'
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'

const Wrapper = styled.div`
  background-color: white;
  border: 1px solid #eaeaea;
  max-width: 1200px;
  margin: 50px auto;
  padding: 50px;
  min-height: 900px;
`

const TitleInput = styled.input`
  margin-bottom: 50px;
  height: 80px;
  font-size: 3em;
  width: 100%;
  font-weight: bold;
  border: none;
  background-color: transparent;
  &:focus {
    outline: none;
  }
`

const SubTitleInput = styled.textarea`
  margin-bottom: 50px;
  width: 100%;
  font-size: 1.5em;
  border: none;
  color: #4c4c4c;
  background-color: transparent;
  &:focus {
    outline: none;
  }
`

class Draft extends React.Component {
  constructor(props) {
    super(props)
    const html = "<p></p>"
    const contentBlock = htmlToDraft(html)
    const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks)

    this.state = {
      editorState: EditorState.createWithContent(contentState),
    };

  }

  componentWillUnmount() {
    this.timeout_ids && this.timeout_ids.forEach(id => clearTimeout(id))
  }

  uploadContent(key, value) {
    if(this.props.ui.spinners.is_saving && !this.props.draft.id) return;
    const id = this.props.draft.id
    const values = key? {[key]: value} : {content_draft: draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))};
    console.log('values: ', values);
    this.props.uploadDraftValuesRequest({id, values, meta: {screen: 'draft', element: 'is_saving'}})
  }

  uploadCallback(file) {
    console.log(file);
    console.log(URL.createObjectURL(file));
    return new Promise(resolve => {
      resolve({data: { link: URL.createObjectURL(file) }})
    })
  }

  onEditorStateChange(editorState) {
    this.setState({editorState})
    if(editorState.getCurrentContent() === this.state.editorState.getCurrentContent()) return;
    this.last_time = Date.now()
    const id = setTimeout(() => {
      if(Date.now() - this.last_time > 900) this.uploadContent()
    }, 1000);
    this.timeout_ids && this.timeout_ids.push(id)
  }

  render() {
    return (
      <Wrapper>
        <TitleInput placeholder="Title here..."
          onBlur={(e) => { this.uploadContent('title_draft', e.currentTarget.value)}}
        />
        <SubTitleInput placeholder="Subtitle here... (Optional)"
          onBlur={(e) => { this.uploadContent('subtitle_draft', e.currentTarget.value)}}
        />
        <Editor ref='editor'
          editorState={this.state.editorState}
          onEditorStateChange={this.onEditorStateChange.bind(this)}
          onBlur={() => this.uploadContent()}
          toolbarClassName="toolbarClassName"
          wrapperClassName="wrapperClassName"
          editorClassName="editorClassName"
          placeholder={"Begin typing..."}
          toolbar={{ image: {
            uploadEnabled: true,
            uploadCallback: this.uploadCallback,
          }}}
        />
      </Wrapper>
    )
  }
}

export default Draft
