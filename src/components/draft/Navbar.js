import React from 'react'
import styled from 'styled-components'
import UserMenu from '../__global__/UserMenu'
import draft from './draft.png'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  background-color: white;
  height: 90px;
  padding: 0 20px;
`

const DraftLogo = styled.div`
  height: 90px;
  width: 140px;
  float: left;
  display: inline-block;
  background-image: url(${draft});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
`

const SaveIndicator = styled.div`
  float: left;
  display: inline-block;
  font-size: 1.3em;
  color: #444444;
  margin-left: 20px;
  line-height: 90px;
`

const Publish = styled.div`
  float: right;
  margin-right: 50px;
  font-size: 1.4em;
  color: #00b262;
  line-height: 100px;
  display: ${({disabled}) => disabled? 'none' : 'block'};
  &:hover {
    cursor: pointer;
    color: #00d173;
  }
`

class Navbar extends React.Component {

  render() {
    const { ui, publishPost, draft_id, ...rest } = this.props

    return (
      <Wrapper>
        <DraftLogo />
        <SaveIndicator>{ ui.spinners.is_saving? 'Saving...' : 'Saved' }</SaveIndicator>
        <UserMenu {...rest}/>
        <Publish disabled={!draft_id} onClick={() => publishPost({id: draft_id})}>Publish</Publish>
      </Wrapper>
    )
  }
}

export default Navbar
