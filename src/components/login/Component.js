import React from 'react'
import styled from 'styled-components'
import Header from './Header'
import Main from './Main'

const Wrapper = styled.div`
  background-color: #efefef;
  min-height: 100%;
`

class Component extends React.Component {
  render() {
    return (
      <Wrapper>
        <Header />
        <Main {...this.props}/>
      </Wrapper>
    )
  }
}

export default Component
