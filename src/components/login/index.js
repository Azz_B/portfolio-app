import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { login, register } from '../../store/actions/auth'

class Container extends React.Component {

  render() {
    return <Component {...this.props}/>
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  loginRequest: login.request,
  registerRequest: register.request,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Container)
