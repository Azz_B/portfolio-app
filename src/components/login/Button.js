import styled from 'styled-components'

const Button = styled.button`
  border: 1px solid ${({disabled}) => disabled? '#b5b5b5' : '#00d6d6'};
  display: block;
  height: 45px;
  width: 150px;
  color: white;
  font-weight: bold;
  background-color: ${({disabled}) => disabled? '#b5b5b5' : '#00c1c1'};
  margin: 30px auto;

  &:hover {
    cursor: pointer;
    background-color: ${({disabled}) => disabled? '#b5b5b5' : '#00d6d6'};
  }
`

export default Button
