import React from 'react'
import { reduxForm, Field } from 'redux-form';
import styled from 'styled-components'
import { Form, Label, Input as SInput} from 'semantic-ui-react';
import Button from './Button'
import { login_validate as validate } from './validate'

const Wrapper = styled.div`
  max-width: 450px;
  margin: 0 auto;
  height: 400px;
  padding: 40px 0;
`
const Input = ({input, placeholder, type, meta: {touched, error}}) => {
  return (
    <div style={{marginBottom: '20px'}}>
      <SInput {...input} placeholder={placeholder} type={type} error={touched && !!error} style={{display: 'block'}}/>
      {(touched && error) && <Label basic color='red' pointing>{error}</Label>}
    </div>
  )
}

class LoginForm extends React.Component {

  submit(values) {
    console.log('values: ', values);
    return new Promise((resolve, reject) => {
      this.props.request({payload: values, meta: {resolve, reject}})
    })
  }

  render() {
    const { handleSubmit, submitting, pristine, anyTouched } = this.props

    return (
      <Wrapper>
        <Form onSubmit={handleSubmit(values => this.submit(values))} loading={submitting}>
          <Field name="login" component={Input} placeholder="Username or Email"/>
          <Field name="password" component={Input} placeholder="Password" type="password"/>
          <Button type="submit" disabled={submitting || pristine}>LOGIN</Button>
        </Form>

      </Wrapper>
    )
  }
}

export default reduxForm({form: 'login', validate})(LoginForm)
