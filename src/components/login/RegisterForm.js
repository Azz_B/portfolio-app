import React from 'react'
import { reduxForm, Field } from 'redux-form';
import styled from 'styled-components'
import { Form, Label, Input as SInput} from 'semantic-ui-react';
import Button from './Button'
import { register_validate as validate } from './validate'

const Wrapper = styled.div`
  max-width: 450px;
  margin: 0 auto;
  min-height: 400px;
  padding: 40px 0;
`

const Input = ({input, placeholder, type, meta: {touched, error}}) => {
  return (
    <div style={{marginBottom: '20px'}}>
      <SInput {...input} placeholder={placeholder} type={type} error={touched && !!error} style={{display: 'block'}}/>
      {(touched && error) && <Label basic color='red' pointing>{error}</Label>}
    </div>
  )
}

class RegisterForm extends React.Component {

  submit(values) {
    return new Promise((resolve, reject) => {
      this.props.request({payload: values, meta: {resolve, reject}})
    })
  }

  render() {
    const { handleSubmit, submitting, pristine, anyTouched } = this.props

    return (
      <Wrapper>
        <Form onSubmit={handleSubmit(values => this.submit(values))} loading={submitting}>
          <Field name="username" component={Input} placeholder="Username"/>
          <Field name="email" component={Input} placeholder="Email"/>
          <Field name="fullname" component={Input} placeholder="Fullname"/>
          <Field name="password" component={Input} placeholder="Password" type="password"/>
          <Field name="confirm_password" component={Input} placeholder="Confirm password" type="password"/>
          <Button type="submit" disabled={submitting || pristine}>REGISTER</Button>
        </Form>
      </Wrapper>
    )
  }
}



export default reduxForm({form: 'register', validate})(RegisterForm)
