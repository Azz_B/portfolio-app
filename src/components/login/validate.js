import { isEmpty, minLength, notEmail } from '../../utils/validations';


export const login_validate = values => {
  const { login, password } = values;
  const errors = {}

  if(isEmpty(login)) errors['login'] = 'login value is required'
  if(isEmpty(password)) errors['password'] = 'password value is required'

  return errors
}

export const register_validate = values => {
  const { username, email, fullname, password, confirm_password } = values
  const errors = {}

  if(isEmpty(username))
    errors.username = 'username value is required';
  else if(minLength(3)(username)) errors.username = 'username must be 3 characters or more';

  if(isEmpty(email)) errors.email = 'email value is required';
  if(notEmail(email)) errors.email = 'email must be a valide email';

  if(isEmpty(password)) errors.password = 'password value is required';
  if(minLength(4)(password)) errors.password = 'password must be 4 characters or more';

  if(confirm_password !== password) errors['confirm_password'] = 'Must be the same of password';

  return errors
}
