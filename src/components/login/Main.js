import React from 'react'
//import PropTypes from 'prop-types'
import styled from 'styled-components'
import LoginForm from './LoginForm'
import RegisterForm from './RegisterForm'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  background-color: white;
  height: 100%;
  max-width: 700px;
  margin: 20px auto;
`

const Box = styled.div`
  border: 1px solid #eaeaea;
  height: ${({height}) => height };
`
const Item = styled.div`
  width: 50%;
  display: inline-block;
  float: left;
  text-align: center;
  line-height: 50px;
  background-color: ${({selected}) => selected? '#f8f8f8' : 'white'};
  border: ${({selected}) => selected? '1px solid #f2f2f2' : 'none'};

  &:hover {
    cursor: pointer;
  }
`

const Navbar = ({handler, selected}) => {
  return (
    <Box height="50px">
      <Item onClick={() => handler(false)} selected={selected} >LOGIN</Item>
      <Item onClick={() => handler(true)} selected={!selected} >REGISTER</Item>
    </Box>
  )
}


class Main extends React.Component {
  constructor(props) {
    super(props)
    this.state = { flag: false }
  }

  render() {
    const { loginRequest, registerRequest, ...props} = this.props

    return (
      <Wrapper>
        <Navbar handler={ flag => this.setState({flag}) } selected={this.state.flag}/>
        {this.state.flag? <RegisterForm {...props} request={registerRequest}/> : <LoginForm {...props} request={loginRequest}/>}
      </Wrapper>
    )
  }
}



export default Main
