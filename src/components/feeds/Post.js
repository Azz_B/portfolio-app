import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  height: 400px;
  max-width: 900px;
  margin: 30px 0;
  margin-left: auto;
  margin-right: auto;
  background-color: white;

  @media screen and (min-width: 1200px) {
    width: 45%;
    margin-left: 2.5%;
    display: inline-block;
  }

  @media screen and (min-width: 1600px) {
    width: 30%;
    display: inline-block;
    margin-left: 2.4%;
  }
`

const Header = styled.div`
  height: 80%;
  background-color: white;

  &:hover {
    cursor: pointer;
  }
`

const Info = styled.div`
  border: 1px solid #eaeaea;
  height: 20%;
  padding: 0 20px;
`

const UserCreator = styled.img`
  border: 1px solid #eaeaea;
  border-radius: 50%;
  height: 60px;
  width: 60px;
  margin-top: 10px;
  display: inline-block;
  float: left;

  &:hover {
    cursor: pointer;
  }
`

const Username = styled.p`
  float: left;
  display: inline-block;
  line-height: 80px;
  margin-left: 20px;
  font-size: 1em;
  @media screen and (min-width: 1600px) {
    font-size: 0.9em;
  }

`
const Title = styled.p`
  margin: 0 20px;
  font-size: 2em;
  text-align: center;
  line-height: 320px;

  @media screen and (min-width: 1600px) {
    font-size: 1.5em;
  }
`

class Post extends React.Component {
  render() {
    console.log('post-props: ', this.props);
    const {post, history} = this.props
    return (
      <Wrapper>
        <Header onClick={() => history.push(`/post/${post.id}`)}>
          <Title>{post.title}</Title>
        </Header>
        <Info>
          <UserCreator src={post.user.photo} onClick={() => history.push(`/profile/${post.user.username}`)}/>
          <Username>{post.user.fullname}</Username>
        </Info>
      </Wrapper>
    )
  }
}


export default Post
