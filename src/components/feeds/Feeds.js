import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import Post from './Post'
import InfiniteScroll from './InfiniteScroll'

const Wrapper = styled.div`
  margin-top: 20px;
`

class Feeds extends React.Component {

  render() {
    const { feeds, ...rest} = this.props
    if(feeds.length < 1) return <div>No Feeds</div>

    return (
      <Wrapper>
        {feeds.map(post => <Post {...rest} key={post.id} post={post}/>)}
      </Wrapper>
    )
  }
}

export default InfiniteScroll(Feeds)
