import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { get_feeds } from '../../store/actions/posts'
import { feedsSelector } from '../../store/selectors/posts'
import { loggedUserSelector } from '../../store/selectors/auth'
import { uiSelector } from '../../store/selectors/ui'

const screen = 'feeds'

class Container extends React.Component {

  componentDidMount() {
    this.loadFeeds()
  }

  loadFeeds = () => {
    const {limit, offset, hasMore} = this.props.ui.config;
    hasMore && this.props.getFeedsRequest({limit, offset})
  }

  render() {
    console.log('feeds-props: ', this.props);
    return <Component {...this.props} loadFeeds={this.loadFeeds}/>
  }
}

const mapStateToProps = (state, props) => ({
  auth: loggedUserSelector(state),
  feeds: feedsSelector(state),
  ui: uiSelector(state, {screen}),
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getFeedsRequest: get_feeds.request,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Container)
