import React from 'react'
import styled from 'styled-components'
import Navbar from './Navbar'
import Feeds from './Feeds'

const Wrapper = styled.div`
  background-color: #fcfcfc;
  padding-top: 80px;
  min-height: 100%;
`

class Component extends React.Component {
  render() {
    const { auth, feeds, ui, loadFeeds, ...rest } = this.props;

    return (
      <Wrapper>
        <Navbar {...rest} auth={auth} />
        <Feeds {...rest} feeds={feeds} load={() => loadFeeds()} loading={ui.spinners.loading}/>
      </Wrapper>
    )
  }
}

export default Component
