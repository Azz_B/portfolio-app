import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { upload_user_photo, update_user } from '../../store/actions/users'
import { currentUserSelector } from '../../store/selectors/users'

class Container extends React.Component {
  render() {
    return <Component {...this.props}/>
  }
}

const mapStateToProps = (state, props) => ({
  user: currentUserSelector(state),
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  upload_user_photo: upload_user_photo.request,
  update_user: update_user.request,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Container)
