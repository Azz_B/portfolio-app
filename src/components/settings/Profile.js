import React from 'react'
import styled from 'styled-components'
import DropPhoto from './DropPhoto'

const Wrapper = styled.div`
  margin: 20px;
  margin-bottom: 101px;
`

const Photo = styled.img`
  border: 1px solid black;
  float: right;
  display: inline-block;
  width: 200px;
  height: 200px;
  border-radius: 50%;
  margin: 20px auto;
  cursor: pointer;

  @media screen and (max-width: 750px) {
    width: 160px;
    height: 160px;
    display: block;
    float: none;
  }
`

const FullName = styled.input`
  border: 1px solid #636363;
  height: 50px;
  width: 70%;
  outline: none;
  font-size: 1.3em;
  padding: 0 10px;
  margin-top: 40px;

  &:focus {
    border-color: #424242;
    border-width: 2px;
  }

  @media screen and (max-width: 750px) {
    display: block;
    width: 100%;
    margin-left: 10px;
    margin-right: 10px;
  }
`

const Bio = styled.textarea`
  border: 1px solid #636363;
  height: 200px;
  width: 70%;
  outline: none;
  font-size: 1.3em;
  padding: 0 10px;
  padding-top: 10px;
  margin-top: 40px;

  &:focus {
    border-color: #424242;
    border-width: 2px;
  }

  @media screen and (max-width: 750px) {
    display: block;
    width: 100%;
    margin-left: 10px;
    margin-right: 10px;
  }
`

const Button = styled.button`
  float: right;
  font-family: "Open Sans", sans-serif;
  background-color: #eee;
  color: #666;
  margin-top: 20px;
  font-size: 1em;
  height: 50px;
  width: 250px;
  padding: 14px 19px 14px;
  letter-spacing: 2px;
  border-radius: 30px;
  transition: .3s;
  outline: none;
  border: 0px;
  cursor: pointer;

  &:hover {
    background-color: #d6d6d6;
  }
`

class Profile extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isDropzoneOpen: false,
      fullname: props.user.fullname,
      bio: props.user.bio,
    }
  }

  onChange(key, e) {
    this.setState({[key]: e.currentTarget.value})
  }

  render() {
    const { upload_user_photo, update_user, user } = this.props

    return (
      <Wrapper>
        <div>
          <Photo src={`http://localhost:4000/api_v1/users/${user.username}/photo`} onClick={() => this.setState({isDropzoneOpen: true}) }/>
          <DropPhoto
            open={this.state.isDropzoneOpen}
            close={() => this.setState({isDropzoneOpen: false})}
            upload_user_photo={upload_user_photo}
          />
          <div>
            <FullName placeholder="FullName" value={this.state.fullname} onChange={(e) => this.onChange('fullname', e)}/>
            <Bio placeholder="Your Bio" value={this.state.bio} onChange={(e) => this.onChange('bio', e)} />
          </div>
          <Button onClick={() => update_user({payload: {fullname: this.state.fullname, bio: this.state.bio}})}>SAVE</Button>
        </div>
      </Wrapper>
    )
  }
}

export default Profile
