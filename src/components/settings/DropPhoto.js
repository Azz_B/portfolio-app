import React from 'react'
import styled from 'styled-components'
import ReactDropzone from 'react-dropzone'

const Wrapper = styled.div`
  border: 1px solid black;
  width: 100%;
  max-width: 1000px;
  height: 100%;
  max-height: 500px;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: white;
  display: ${ ({open}) => open? 'block' : 'none' };
`

const Dropzone = styled(ReactDropzone)`
  width: 100%;
  height: 100%;
`
const CloseButton = styled.div`
  height: 30px;
  width: 30px;
  background-color: gray;
  color: white;
  font-size: 1.5em;
  padding-top: 5px;
  position: absolute;
  top: 0px;
  right: 0px;
  text-align: center;
  cursor: pointer;
`


class DropPhoto extends React.Component {

  onDrop(files) {
    this.props.upload_user_photo({file: files[0]})
  }

  render() {
    const { close, open } = this.props

    return (
      <Wrapper open={open}>
        <CloseButton onClick={() => close && close()}>X</CloseButton>
        <Dropzone onDrop={this.onDrop.bind(this)} >
          <p>Drop the Photo here</p>
        </Dropzone>
      </Wrapper>
    )
  }
}

export default DropPhoto
