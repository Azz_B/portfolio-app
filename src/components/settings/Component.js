import React from 'react'
import styled from 'styled-components'
import { Divider } from 'semantic-ui-react'
import Profile from './Profile'

const Wrapper = styled.div`
  max-width: 1000px;
  margin: 40px auto;
`

class Component extends React.Component {
  render() {
    const { upload_user_photo, update_user, user } = this.props

    return (
      <Wrapper>
        <Divider horizontal> Profile Settings</Divider>
        <Profile upload_user_photo={upload_user_photo} update_user={update_user} user={user}/>
        <Divider horizontal> Email Settings</Divider>
      </Wrapper>
    )
  }
}

export default Component
