import React from 'react'
import { Route } from 'react-router-dom';
import NotMatch from './NotMatch'

function LoggedRoute(props) {
  const { component, auth, ui, ...rest } = props
  console.log('LoggedRoute-props: ', props);
  let Component;

  if(ui.spinners.loading)
    Component = () => <div>loading...</div>
  else if(!props.auth.username)
    Component = NotMatch;
  else
    Component = component

  return <Route {...rest} component={Component} />
}

export default LoggedRoute
