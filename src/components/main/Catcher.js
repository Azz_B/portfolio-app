import React from 'react'

class Catcher extends React.Component {

  componentDidCatch(error, errorInfo) {
    console.log('Catcher: ', error);
    console.log('Catcher: ', errorInfo);
  }

  render() {
    return this.props.children
  }
}

export default Catcher
