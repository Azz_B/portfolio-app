import React from 'react'
import { Route } from 'react-router-dom';
import NotMatch from './NotMatch'

function NotLoggedRoute(props) {
  const { component, auth, ui, ...rest } = props
  console.log('NotLoggedRoute-props: ', props);
  let Component;

  if(ui.spinners.loading)
    Component = () => <div>loading...</div>
  else if(auth.username)
    Component = NotMatch;
  else
    Component = component

  return <Route {...rest} component={Component} />
}

export default NotLoggedRoute
