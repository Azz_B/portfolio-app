import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { loggedUserSelector } from '../../store/selectors/auth'
import { uiSelector } from '../../store/selectors/ui'
import Portfolio from '../portfolio'
import Login from '../login'
import Feeds from '../feeds'
import Profile from '../profile'
import Post from '../post'
import Draft from '../draft'
import Settings from '../settings'
import NotMatch from './NotMatch'
import NotLoggedRoute from './NotLoggedRoute'
import LoggedRoute from './LoggedRoute'

class Main extends Component {

  render() {
    const { auth, ui } = this.props

    return (
      <Switch>
        <Route exact path="/" component={Portfolio}/>
        <NotLoggedRoute exact path="/login" component={Login} auth={auth} ui={ui} />
        <Route exact path="/feeds" component={Feeds}/>
        <Route exact path="/profile/:username" component={Profile} />
        <Route exact path="/post/:id" component={Post} />
        <LoggedRoute exact path="/post/:id/edit" component={Draft} is_edit />
        <LoggedRoute exact path="/new-post" component={Draft} auth={auth} ui={ui} />
        <LoggedRoute exact path="/settings" component={Settings} auth={auth} ui={ui} />
        <Route render={ () => <NotMatch /> } />
      </Switch>
    )
  }
}

const mapStateToProps = (state) => ({
  auth: loggedUserSelector(state),
  ui: uiSelector(state, {screen: 'auth'}),
})

export default connect(mapStateToProps)(Main)
