import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'


const Wrapper = styled.p`
  background-color: white;
  min-height: 900px;
  max-width: 1200px;
  padding-top: 100px;
  padding-left: 30px;
  padding-right: 20px;
  padding-bottom: 50px;
  margin: 0 auto;
  margin-top: 0px;
  line-height: 300%;

`

class Content extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {

    return (
      <Wrapper dangerouslySetInnerHTML={{__html: this.props.content}}>

      </Wrapper>
    )
  }
}

export default Content
