import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'


const Wrapper = styled.div`
  background-color: white;
  min-height: 100px;
  max-width: 1200px;
  padding: 20px 20px;
  margin: 0 auto;
  margin-top: 150px;
  margin-bottom: 90px;
  text-align: center;
`

const Title = styled.p`
  font-size: 3em;
  margin-top: 10px;
`
const Subtitle = styled.p`
  font-size: 1.2em;
  color: gray;
  margin-top: 30px;
`

class Intro extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { title, subtitle } = this.props
    return (
      <Wrapper>
        <Title>{title}</Title>
        <Subtitle>{subtitle}</Subtitle>
      </Wrapper>
    )
  }
}

export default Intro
