import React from 'react'
import styled from 'styled-components'
import Navbar from './Navbar'
import User from './User'
import Intro from './Intro'
import Content from './Content'
import Comments from './comments'

const Wrapper = styled.div`
  background-color: white;
  padding-top: 80px;
  min-height: 100%;
`

class Component extends React.Component {
  render() {
    const { auth, post, ui, comments, createCommentRequest, ...rest } = this.props
    window.LOG('props', this.props)
    if(ui.spinners.loading || !post.user) return <Wrapper><Navbar {...rest} auth={auth}/></Wrapper>

    return (
      <Wrapper>
        <Navbar {...rest} auth={auth}/>
        <User {...rest} user={post.user}/>
        <Intro title={post.title} subtitle={post.subtitle}/>
        <Content content={post.content} />
        <Comments {...rest} post_id={post.id} comments={comments} comments_count={post.comments_count} createComment={createCommentRequest}/>
      </Wrapper>
    )
  }
}

export default Component
