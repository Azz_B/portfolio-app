import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Component from './Component'
import { get_post } from '../../store/actions/posts'
import { postSelector } from '../../store/selectors/posts'
import { get_post_comments, create_comment } from '../../store/actions/comments'
import { commentsSelector } from '../../store/selectors/comments'
import { loggedUserSelector } from '../../store/selectors/auth'
import { uiSelector } from '../../store/selectors/ui'

const screen = 'post'

class Container extends React.Component {

  componentDidMount() {
    const id = this.props.match.params.id
    this.props.getPostRequest({id, meta: {screen}})
    this.props.getPostCommentsRequest({post_id: id, meta: {screen}})
  }

  render() {
    console.log('comments: ', this.props);
    return <Component {...this.props}/>
  }
}

const mapStateToProps = (state, props) => ({
  auth: loggedUserSelector(state),
  post: postSelector(state, {id: props.match.params.id}),
  comments: commentsSelector(state, {post_id: props.match.params.id}),
  ui: uiSelector(state, {screen}),
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getPostRequest: get_post.request,
  getPostCommentsRequest: get_post_comments.request,
  createCommentRequest: create_comment.request,
}, dispatch)


export default connect(mapStateToProps, mapDispatchToProps)(Container)
