import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'

const Wrapper = styled.div`
  border-bottom: 1px solid #eaeaea;
  background-color: white;
  height: 90px;
  max-width: 1200px;
  padding: 0 20px;
  margin: 0 auto;
  margin-top: 150px;
`

const Avatar = styled.img`
  border: 1px solid #eaeaea;
  border-radius: 50%;
  float: left;
  height: 50px;
  width: 50px;
  margin-top: 10px;
  display: inline-block;
  cursor: pointer;
`

const Username = styled.span`
  font-size: 1em;
  display: inline-block;
  float: left;
  line-height: 75px;
  margin-left: 20px;
`

class User extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { user, history, style } = this.props
    return (
      <Wrapper style={style}>
        <Avatar src={user.photo} onClick={() => history.push(`/profile/${user.username}`)} />
        <Username>{user.fullname}</Username>
      </Wrapper>
    )
  }
}

export default User
