import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import List from './List'
import Form from './Form'

const Wrapper = styled.div`
  border-top: 1px solid #eaeaea;
  background-color: white;
  min-height: 400px;
  max-width: 1200px;
  padding: 20px 20px;
  margin: 0 auto;
  margin-top: 10px;
`

class Component extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {post_id, createComment, comments} = this.props

    return (
      <Wrapper>
        <p style={{fontSize: '1.1em', color: '#515151', fontWeight: 'bold'}}>Comments {this.props.comments_count}</p>
        <Form createComment={createComment} post_id={post_id}/>
        <List comments={comments}/>
      </Wrapper>
    )
  }
}

export default Component
