import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'


const Wrapper = styled.div`
  background-color: white;
  min-height: 200px;
  max-width: 1000px;
  margin-top: 0px;
  padding-top: 20px;
`

const Avatar = styled.div`
  float: left;
  border: 1px solid #777777;
  border-radius: 50%;
  height: 50px;
  width: 50px;
  margin-top: 10px;
  display: inline-block;
`

const Username = styled.span`
  font-size: 1em;
  display: inline-block;
  line-height: 75px;
  margin-left: 20px;
`

const Content = styled.p`
  font-size: 1.3em;
  padding-left: 15px;
  border-left: 3px solid gray;
  color: #515151;
`

const Replies = styled.div`
  padding-left: 20px;
  border-left: 3px solid gray;
`

class Comment extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const {comment} = this.props;
    return (
      <Wrapper >
        <div style={{display: 'block'}}><Avatar /> <Username>{comment.user.fullname}</Username></div>
        <Content>{comment.content}</Content>
        <Replies>{this.props.children}</Replies>
      </Wrapper>
    )
  }
}

export default Comment
