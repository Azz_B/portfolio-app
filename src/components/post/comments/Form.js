import React from 'react'
import styled from 'styled-components'

const Input = styled.textarea`
  margin-top: 30px;
  min-width: 900px;
  height: 200px;
  font-size: 1.6em;
  padding: 20px;
  outline: none;
  display: block;

  &:focus {
    border-color: #515151;
  }
`

const Button = styled.button`
  font-family: "Open Sans", sans-serif;
  background-color: #eee;
  color: #666;
  margin-top: 20px;
  font-size: 1em;
  height: 50px;
  width: 250px;
  padding: 14px 19px 14px;
  letter-spacing: 2px;
  border-radius: 30px;
  transition: .3s;
  outline: none;
  border: 0px;
  cursor: pointer;

  &:hover {
    background-color: #d6d6d6;
  }
`

class Form extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: '',
    }
  }

  onClick() {
    if(!this.state.value) return;
    const { createComment, post_id } = this.props
    this.props.createComment({post_id, payload: {content: this.state.value}})
    this.setState({value: ''})
  }

  render() {
    return (
      <div>
        <Input placeholder="Comment here" value={this.state.value} onChange={(e) => this.setState({value: e.currentTarget.value})} />
        <Button onClick={this.onClick.bind(this)}>POST COMMENT</Button>
      </div>
    )
  }
}

export default Form
