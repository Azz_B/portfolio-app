import React from 'react'
import ReactDOM from 'react-dom'
import styled from 'styled-components'
import Comment from './Comment'

const Wrapper = styled.div`
  background-color: white;
  min-height: 400px;
  max-width: 1200px;
  margin-top: 40px;
`

const SeeMore = styled.div`
  border: 1px solid #565656;
  height: 40px;
  text-align: center;
  line-height: 40px;
  font-size: 1.2em;
  color: #565656;
  cursor: pointer;
`

class List extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {

    return (
      <Wrapper >
        {this.props.comments.map(comment => <Comment key={comment.id} comment={comment}/>)}
        <SeeMore>Load More Comments</SeeMore>
      </Wrapper>
    )
  }
}

export default List
