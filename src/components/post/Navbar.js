import React from 'react'
import styled from 'styled-components'
import UserMenu from '../__global__/UserMenu'
import logo from './logo.png'

const Wrapper = styled.div`
  border: 1px solid #eaeaea;
  background-color: white;
  height: 90px;
  padding: 0 20px;
  position: fixed;
  top: 0;
  width: 100%;
`
const Logo = styled.div`
  float: left;
  height: 75px;
  width: 150px;
  margin-top: 2px;
  background-image: url(${logo});
  background-repeat: no-repeat;
  background-position: center;
  background-size: contain;
  cursor: pointer;
`
const LoginLink = styled.div`
  float: right;
  border: 1px solid #fe6b6b;
  width: 150px;
  height: 50px;
  margin-top: 15px;
  line-height: 55px;
  text-align: center;
  font-size: 1.2em;
  font-weight: bold;
  color: white;
  background-color: #fe6b6b;

  &:hover {
    cursor: pointer;
    opacity: 0.9;
  }
`

class Navbar extends React.Component {

  render() {
    const { auth, history } = this.props

    return (
      <Wrapper>
        <Logo onClick={() => history.push('/feeds')} />
        {auth.username? <UserMenu {...this.props}/> : <LoginLink onClick={() => history.push('/login')}>LOGIN</LoginLink>}
      </Wrapper>
    )
  }
}

export default Navbar
