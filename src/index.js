import './load'
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';
import Main from './components/main/Main'
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store'

const history = createBrowserHistory()
const store = configureStore(history)

const root = document.getElementById('root');

const render = () => {
  ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <Main />
      </ConnectedRouter>
    </Provider>,
    root
  );
}

render()
registerServiceWorker();

if(module.hot) {
  module.hot.accept("./components/main/Main", () => {
    setTimeout(render)
  })
}
