const namespacedAction = (action) => `Comments/${action}`;
const createAction = (type) => (payload = {}) => ({ type, ...payload});

export const GET_POST_COMMENTS_REQUEST = namespacedAction('GET_POST_COMMENTS_REQUEST')
export const GET_POST_COMMENTS_SUCCESS = namespacedAction('GET_POST_COMMENTS_SUCCESS')
export const GET_POST_COMMENTS_FAILED = namespacedAction('GET_POST_COMMENTS_FAILED')

export const get_post_comments = {
  request: createAction(GET_POST_COMMENTS_REQUEST),
  success: createAction(GET_POST_COMMENTS_SUCCESS),
  failed: createAction(GET_POST_COMMENTS_FAILED),
}

export const CREATE_COMMENT_REQUEST = namespacedAction('CREATE_COMMENT_REQUEST')
export const CREATE_COMMENT_SUCCESS = namespacedAction('CREATE_COMMENT_SUCCESS')
export const CREATE_COMMENT_FAILED = namespacedAction('CREATE_COMMENT_FAILED')

export const create_comment = {
  request: createAction(CREATE_COMMENT_REQUEST),
  success: createAction(CREATE_COMMENT_SUCCESS),
  failed: createAction(CREATE_COMMENT_FAILED),
}
