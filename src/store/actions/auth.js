const namespacedAction = (action) => `Auth/${action}`;
const createAction = (type) => (payload = {}) => ({ type, ...payload});

export const LOGIN_REQUEST = namespacedAction('LOGIN_REQUEST')
export const LOGIN_SUCCESS = namespacedAction('LOGIN_SUCCESS')
export const LOGIN_FAILED = namespacedAction('LOGIN_FAILED')
export const LOGIN_BY_TOKEN_REQUEST = namespacedAction('LOGIN_BY_TOKEN_REQUEST')

export const login = {
  request: createAction(LOGIN_REQUEST),
  success: createAction(LOGIN_SUCCESS),
  failed: createAction(LOGIN_FAILED),
  request_by_token: createAction(LOGIN_BY_TOKEN_REQUEST),
}

export const REGISTER_REQUEST = namespacedAction('REGISTER_REQUEST')
export const REGISTER_SUCCESS = namespacedAction('REGISTER_SUCCESS')
export const REGISTER_FAILED = namespacedAction('REGISTER_FAILED')

export const register = {
  request: createAction(REGISTER_REQUEST),
  success: createAction(REGISTER_SUCCESS),
  failed: createAction(REGISTER_FAILED),
}

export const LOGOUT_REQUEST = namespacedAction('LOGOUT_REQUEST')
export const LOGOUT_SUCCESS = namespacedAction('LOGOUT_SUCCESS')

export const logout = {
  request: createAction(LOGOUT_REQUEST),
  success: createAction(LOGOUT_SUCCESS),
}
