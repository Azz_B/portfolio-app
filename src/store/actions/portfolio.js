const namespacedAction = (action) => `Portfolio/${action}`;
const createAction = (type) => (payload = {}) => ({ type, ...payload});


export const MAIN_PORTFOLIO_REQUEST = namespacedAction('MAIN_PORTFOLIO_REQUEST')
export const MAIN_PORTFOLIO_SUCCESS = namespacedAction('MAIN_PORTFOLIO_SUCCESS')


export const mainPortfolio = {
  request: createAction(MAIN_PORTFOLIO_REQUEST),
  success: createAction(MAIN_PORTFOLIO_SUCCESS),
}
