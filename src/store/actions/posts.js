const namespacedAction = (action) => `Posts/${action}`;
const createAction = (type) => (payload = {}) => ({ type, ...payload});

export const GET_POST_REQUEST = namespacedAction('GET_POST_REQUEST')
export const GET_POST_SUCCESS = namespacedAction('GET_POST_SUCCESS')
export const GET_POST_FAILED = namespacedAction('GET_POST_FAILED')

export const get_post = {
  request: createAction(GET_POST_REQUEST),
  success: createAction(GET_POST_SUCCESS),
  failed: createAction(GET_POST_FAILED),
}

export const GET_FEEDS_REQUEST = namespacedAction('GET_FEEDS_REQUEST')
export const GET_FEEDS_SUCCESS = namespacedAction('GET_FEEDS_SUCCESS')
export const GET_FEEDS_FAILED = namespacedAction('GET_FEEDS_FAILED')

export const get_feeds = {
  request: createAction(GET_FEEDS_REQUEST),
  success: createAction(GET_FEEDS_SUCCESS),
  failed: createAction(GET_FEEDS_FAILED),
}

export const GET_USER_POSTS_REQUEST = namespacedAction('GET_USER_POSTS_REQUEST')
export const GET_USER_POSTS_SUCCESS = namespacedAction('GET_USER_POSTS_SUCCESS')
export const GET_USER_POSTS_FAILED = namespacedAction('GET_USER_POSTS_FAILED')

export const get_user_posts = {
  request: createAction(GET_USER_POSTS_REQUEST),
  success: createAction(GET_USER_POSTS_SUCCESS),
  failed: createAction(GET_USER_POSTS_FAILED),
}

export const UPLOAD_DRAFT_VALUES_REQUEST = namespacedAction('UPLOAD_DRAFT_VALUES_REQUEST')
export const UPLOAD_DRAFT_VALUES_SUCCESS = namespacedAction('UPLOAD_DRAFT_VALUES_SUCCESS')
export const UPLOAD_DRAFT_VALUES_FAILED = namespacedAction('UPLOAD_DRAFT_VALUES_FAILED')

export const uploadDraftValues = {
  request: createAction(UPLOAD_DRAFT_VALUES_REQUEST),
  success: createAction(UPLOAD_DRAFT_VALUES_SUCCESS),
  failed: createAction(UPLOAD_DRAFT_VALUES_FAILED),
}

export const PUBLISH_POST_REQUEST = namespacedAction('PUBLISH_POST_REQUEST')
export const PUBLISH_POST_SUCCESS = namespacedAction('PUBLISH_POST_SUCCESS')
export const PUBLISH_POST_FAILED = namespacedAction('PUBLISH_POST_FAILED')

export const publishPost = {
  request: createAction(PUBLISH_POST_REQUEST),
  success: createAction(PUBLISH_POST_SUCCESS),
  failed: createAction(PUBLISH_POST_FAILED),
}

// export const SELECT_DRAFT = namespacedAction('SELECT_DRAFT')
// export const selectDraft = createAction(SELECT_DRAFT)
