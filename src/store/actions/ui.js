const namespacedAction = (action) => `UI/${action}`;
const createAction = (type) => (payload = {}) => ({ type, ...payload});

export const SET_SPINNER = namespacedAction('SET_SPINNER')
export const setSpinner = createAction(SET_SPINNER)

export const SET_CONFIG = namespacedAction('SET_CONFIG')
export const setConfig = createAction(SET_CONFIG)
