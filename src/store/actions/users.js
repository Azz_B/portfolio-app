const namespacedAction = (action) => `Users/${action}`;
const createAction = (type) => (payload = {}) => ({ type, ...payload});

export const GET_USER_REQUEST = namespacedAction('GET_USER_REQUEST')
export const GET_USER_SUCCESS = namespacedAction('GET_USER_SUCCESS')
export const GET_USER_FAILED = namespacedAction('GET_USER_FAILED')

export const get_user = {
  request: createAction(GET_USER_REQUEST),
  success: createAction(GET_USER_SUCCESS),
  failed: createAction(GET_USER_FAILED),
}

export const UPDATE_USER_REQUEST = namespacedAction('UPDATE_USER_REQUEST')
export const UPDATE_USER_SUCCESS = namespacedAction('UPDATE_USER_SUCCESS')
export const UPDATE_USER_FAILED = namespacedAction('UPDATE_USER_FAILED')

export const update_user = {
  request: createAction(UPDATE_USER_REQUEST),
  success: createAction(UPDATE_USER_SUCCESS),
  failed: createAction(UPDATE_USER_FAILED),
}

export const UPLOAD_USER_PHOTO_REQUEST = namespacedAction('UPLOAD_USER_PHOTO_REQUEST')
export const UPLOAD_USER_PHOTO_SUCCESS = namespacedAction('UPLOAD_USER_PHOTO_SUCCESS')
export const UPLOAD_USER_PHOTO_FAILED = namespacedAction('UPLOAD_USER_PHOTO_FAILED')

export const upload_user_photo = {
  request: createAction(UPLOAD_USER_PHOTO_REQUEST),
  success: createAction(UPLOAD_USER_PHOTO_SUCCESS),
  failed: createAction(UPLOAD_USER_PHOTO_FAILED),
}
