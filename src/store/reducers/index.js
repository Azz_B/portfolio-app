import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form'
import auth from './auth'
import portfolios, { main_portfolio } from './portfolios'
import users from './users'
import posts, { feeds, selected_draft, drafts } from './posts'
import comments from './comments'
import ui from './ui'

export default combineReducers({
  auth,
  main_portfolio,
  portfolios,
  users,
  posts,
  selected_draft,
  drafts,
  comments,
  feeds,
  ui,
  form,
})
