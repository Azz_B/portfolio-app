import {
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
  LOGOUT_SUCCESS,
} from '../actions/auth'

const initialState = {
  username: null,
}

export default function auth(state = initialState, action) {
  switch (action.type) {
    case REGISTER_SUCCESS:
    case LOGIN_SUCCESS: {
        return {...state, username: action.user.username}
    }

    case LOGOUT_SUCCESS: {
      return initialState;
    }

    default:
        return state
  }
}
