import {
  MAIN_PORTFOLIO_SUCCESS,
} from '../actions/portfolio'

const initialState = {}

export default function portfolios(state = initialState, action) {
  switch (action.type) {
    case MAIN_PORTFOLIO_SUCCESS: {
      return {...state, [action.portfolio.id]: { ...action.portfolio }}
    }

    default:
      return state
  }
}


export function main_portfolio(state = null, action) {
  switch (action.type) {
    case MAIN_PORTFOLIO_SUCCESS: {
      return action.portfolio.id
    }

    default:
      return state
  }
}
