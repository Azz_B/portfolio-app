import {
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
} from '../actions/auth'
import {
  GET_USER_SUCCESS,
  UPDATE_USER_SUCCESS,
  UPLOAD_USER_PHOTO_SUCCESS,
} from '../actions/users'

const initialState = {}

export default function users(state = initialState, action) {
  switch (action.type) {
    case UPLOAD_USER_PHOTO_SUCCESS:
    case UPDATE_USER_SUCCESS:
    case GET_USER_SUCCESS:
    case REGISTER_SUCCESS:
    case LOGIN_SUCCESS: {
      return { ...state, [action.user.username]: action.user }
    }

    default:
      return state;
  }
}
