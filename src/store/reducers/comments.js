import {
  GET_POST_COMMENTS_SUCCESS,
  CREATE_COMMENT_SUCCESS,
} from '../actions/comments'

const initialState = {}

export default function comments(state = initialState, action) {
  switch (action.type) {
    case GET_POST_COMMENTS_SUCCESS: {
      const comments = {}
      action.comments.forEach(comment => comments[comment.id] = comment)
      return { ...state, ...comments}
    }

    case CREATE_COMMENT_SUCCESS: {
      return {...state, [action.comment.id]: action.comment}
    }

    default:
      return state
  }
}
