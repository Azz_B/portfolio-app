import {
  GET_POST_SUCCESS,
  GET_FEEDS_SUCCESS,
  GET_USER_POSTS_SUCCESS,
  UPLOAD_DRAFT_VALUES_SUCCESS,
} from '../actions/posts'

const initialState = {}

export default function posts(state = initialState, action) {
  switch (action.type) {
    case GET_POST_SUCCESS: {
      return { ...state, [action.post.id]: action.post }
    }

    case GET_USER_POSTS_SUCCESS:
    case GET_FEEDS_SUCCESS: {
      const next_posts = {}
      action.posts.forEach(post => next_posts[post.id] = post)
      return {...state, ...next_posts}
    }

    default:
      return state
  }
}


export function feeds(state = { posts: {} }, action) {
  switch (action.type) {
    case GET_FEEDS_SUCCESS: {
      const next_posts = {}
      action.posts.forEach(post => next_posts[post.id] = true)
      return { ...state, posts: { ...state.posts,  ...next_posts} }
    }

    default:
      return state
  }
}

export function selected_draft(state = null, action) {
  if(action.type === UPLOAD_DRAFT_VALUES_SUCCESS) return action.post.id
  return state
}

export function drafts(state = {}, action) {
  switch (action.type) {
    case UPLOAD_DRAFT_VALUES_SUCCESS: {
      return { ...state, [action.post.id]: action.post }
    }

    default:
      return state
  }
}
