import { SET_SPINNER, SET_CONFIG } from '../actions/ui'

const initialState = {
  auth: {
    spinners: {
      loading: false,
    },
  },

  profile: {
    spinners: {
      userInfo: false,
      posts: false,
      drafts: false,
    },
    config: {
      posts_limit: 5,
      posts_offset: 0,
      posts_hasMore: true,
      drafts_limit: 5,
      drafts_offset: 0,
      drafts_hasMore: true,
    },
  },

  draft: {
    spinners: {
      is_saving: false,
    },
  },

  feeds: {
    spinners: {
      loading: false,
    },
    config: {
      limit: 10,
      offset: 0,
      hasMore: true,
    },
  },

  post: {
    spinners: {
      loading: false,
      comments_loading: false,
    },
  },

  // comments: {
  //   spinners: {
  //     loading: false,
  //   },
  // }
}

export default function ui(state = initialState, action) {
  switch (action.type) {
    case SET_SPINNER: {
      const { screen, element, visible } = action
      const screen_data = {...state[screen], spinners: { ...state[screen]['spinners'], [element]: visible }}
      return { ...state, [screen]: screen_data }
    }

    case SET_CONFIG: {
      const { screen, config } = action
      const screen_data = { ...state[screen],  config: {...state[screen]['config'], ...config } }
      return { ...state, [screen]: screen_data }
    }

    default:
      return state
  }
}
