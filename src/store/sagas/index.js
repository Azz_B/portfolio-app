import { fork, all } from 'redux-saga/effects';
import init from './init'
import authWatch from './auth'
import portfoliosWatch from './portfolio'
import usersWatch from './users'
import postsWatch from './posts'
import commentsWatch from './comments'

export default function* root() {
  yield all([
    fork(authWatch),
    fork(portfoliosWatch),
    fork(usersWatch),
    fork(postsWatch),
    fork(commentsWatch),
    fork(init),
  ])
}
