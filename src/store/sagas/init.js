import { all, put, fork } from 'redux-saga/effects'
import {
  login,
  logout,
} from '../actions/auth'
import { setSpinner } from '../actions/ui'

export default function* init() {
  yield all([
    fork(loadLoggedUser),
  ])
}


function* loadLoggedUser() {
  const screen = 'auth', element = 'loading';
  //localStorage.removeItem('token@my-portfolio-app')
  yield put(setSpinner({screen, element, visible: true}))
  const token = localStorage.getItem('token@my-portfolio-app');
  const action = token ? login.request_by_token({token}) : logout.request()
  yield put(action)
  yield put(setSpinner({screen, element, visible: false}))
}
