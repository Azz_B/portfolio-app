import { takeLatest, all, put } from 'redux-saga/effects'
import { delay } from 'redux-saga';
import Api from '../endpoints/api/users'
import {
  GET_USER_REQUEST,
  get_user,
  UPDATE_USER_REQUEST,
  update_user,
  UPLOAD_USER_PHOTO_REQUEST,
  upload_user_photo,
} from '../actions/users'
import { setSpinner } from '../actions/ui'

export default function* usersWatch() {
  yield all([
    takeLatest(GET_USER_REQUEST, getUserRequest),
    takeLatest(UPDATE_USER_REQUEST, updateUserRequest),
    takeLatest(UPLOAD_USER_PHOTO_REQUEST, uploadUserPhotoRequest),
  ])
}


function* getUserRequest({username, meta: { screen, element }}) {
  try {
    yield put(setSpinner({screen, element, visible: true}))
    //yield delay(2000)
    const data = yield Api.get_user(username)
    yield put(get_user.success(data))
  } catch (e) {
    yield put(get_user.failed())
  } finally {
    yield put(setSpinner({screen, element, visible: false}))
  }
}

function* updateUserRequest({payload}) {
  console.log('payload: ', payload);
  try {
    const token = localStorage.getItem('token@my-portfolio-app');
    const data = yield Api.update_user({user: payload}, token)
    yield put(update_user.success(data))
  } catch (e) {
    yield put(update_user.failed())
  }
}

function* uploadUserPhotoRequest({file}) {
  try {
    const token = localStorage.getItem('token@my-portfolio-app');
    const body = yield new Promise(
      (resolve) => {
        const reader = new FileReader(file)
        reader.onload = (e) => {
          const form = new FormData()
          form.append('photo', file)
          resolve(form)
        }
        reader.readAsBinaryString(file)
      }
    );

    const data = yield Api.upload_user_photo(body, token)
    yield put(upload_user_photo.success(data))
  } catch (e) {
    yield put(upload_user_photo.failed())
  }
}
