import { takeLatest, all, put } from 'redux-saga/effects'
import { push } from 'connected-react-router';
import Api from '../endpoints/api/comments'
import {
  GET_POST_COMMENTS_REQUEST,
  get_post_comments,
  CREATE_COMMENT_REQUEST,
  create_comment,
} from '../actions/comments'
import { setSpinner } from '../actions/ui'

export default function* commentsWatch() {
  yield all([
    takeLatest(GET_POST_COMMENTS_REQUEST, getPostCommentsRequest),
    takeLatest(CREATE_COMMENT_REQUEST, createCommentRequest),
  ])
}

function* getPostCommentsRequest({post_id, meta: {screen}}) {
  const element = 'comments_loading'
  try {
    yield put(setSpinner({screen, element, visible: true}))
    const data = yield Api.get_post_comments(post_id)
    yield put(get_post_comments.success(data))
  } catch (e) {
    yield put(get_post_comments.failed())
  } finally {
    yield put(setSpinner({screen, element, visible: false}))
  }
}

function* createCommentRequest({post_id, payload}) {
  try {
    const token = localStorage.getItem('token@my-portfolio-app');
    const data = yield Api.create_comment(post_id, {comment: payload}, token)
    yield put(create_comment.success(data))
  } catch (e) {
    yield put(create_comment.failed())
  } finally {

  }
}
