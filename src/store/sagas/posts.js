import { takeLatest, takeEvery, all, put } from 'redux-saga/effects'
import { delay } from 'redux-saga';
import { push } from 'connected-react-router';
import Api from '../endpoints/api/posts'
import {
  GET_POST_REQUEST,
  get_post,
  GET_FEEDS_REQUEST,
  get_feeds,
  GET_USER_POSTS_REQUEST,
  get_user_posts,
  UPLOAD_DRAFT_VALUES_REQUEST,
  uploadDraftValues,
  PUBLISH_POST_REQUEST,
} from '../actions/posts'
import { setSpinner, setConfig } from '../actions/ui'

export default function* postsWatch() {
  yield all([
    takeLatest(GET_POST_REQUEST, getPostRequest),
    takeLatest(GET_FEEDS_REQUEST, getFeedsRequest),
    takeLatest(GET_USER_POSTS_REQUEST, getUserPostsRequest),
    takeEvery(UPLOAD_DRAFT_VALUES_REQUEST, uploadDraftValuesRequest),
    takeLatest(PUBLISH_POST_REQUEST, publishPostRequest),
  ])
}


function* getPostRequest({id, meta: {screen}}) {
  const element = 'loading';
  try {
    yield put(setSpinner({screen, element, visible: true}))
    const data = yield Api.get_post(id)
    yield put(get_post.success(data))
  } catch (e) {
    yield put(get_post.failed())
  } finally {
    yield put(setSpinner({screen, element, visible: false}))
  }
}

function* getFeedsRequest({limit, offset}) {
  const screen = 'feeds'
  const element = 'loading'
  try {
    yield put(setSpinner({screen, element, visible: true}))
    const token = localStorage.getItem('token@my-portfolio-app');
    const data = yield Api.get_feeds(limit, offset, token)
    window.LOG('feeds data', data);
    yield put(get_feeds.success(data))
    yield put(setConfig({screen, config: data.config }))
  } catch (e) {
    yield put(get_feeds.failed())
  } finally {
    yield put(setSpinner({screen, element, visible: false}))
  }
}

function* getUserPostsRequest({username, meta: { screen, element }}) {
  try {
    yield put(setSpinner({screen, element, visible: true}))
    const data = yield Api.get_user_posts(username)
    yield put(get_user_posts.success(data))
  } catch (e) {
    yield put(get_user_posts.failed())
  } finally {
    yield put(setSpinner({screen, element, visible: false}))
  }
}

function* uploadDraftValuesRequest({id, values, meta: {screen, element}}) {
  try {
    yield put(setSpinner({screen, element, visible: true}))
    //yield delay(1000)
    const token = localStorage.getItem('token@my-portfolio-app');
    const data = id? yield Api.update_post(id, {post: values}, token) : yield Api.create_post({post: values}, token)
    yield put(uploadDraftValues.success(data))
  } catch (e) {
    console.log(e);
    yield put(uploadDraftValues.failed())
  } finally {
    yield put(setSpinner({screen, element, visible: false}))
  }
}

function* publishPostRequest({id}) {
  try {
    const token = localStorage.getItem('token@my-portfolio-app');
    const data = yield Api.publish_post(id, token)
    console.log('after-publish: ', data);
    yield put(push(`/post/${id}`))
  } catch (e) {
    console.log(e);
  }
}
