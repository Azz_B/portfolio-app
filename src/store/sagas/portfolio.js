import { takeLatest, all, put } from 'redux-saga/effects'
import Api from '../endpoints/api/portfolio'
import {
  MAIN_PORTFOLIO_REQUEST,
  MAIN_PORTFOLIO_SUCCESS,
  mainPortfolio,
} from '../actions/portfolio'

export default function* portfolioWatch() {
  yield all([
    takeLatest(MAIN_PORTFOLIO_REQUEST, mainPortfolioRequest),
  ])
}


function* mainPortfolioRequest() {
  try {
    const data = yield Api.mainPortfolio()
    yield put(mainPortfolio.success(data))
  } catch (e) {
    console.log(e);
  }
}
