import { takeLatest, all, put, take, cancel, fork, cancelled } from 'redux-saga/effects'
import { push } from 'connected-react-router';
import { SubmissionError } from 'redux-form';
import HttpError from '../endpoints/HttpError'
import Api from '../endpoints/api/auth'
import {
  LOGIN_REQUEST,
  LOGIN_BY_TOKEN_REQUEST,
  LOGIN_FAILED,
  REGISTER_REQUEST,
  LOGOUT_REQUEST,
  login,
  register,
  logout,
} from '../actions/auth'

export default function* authWatch() {
  yield all([
    fork(loginFlow),
    takeLatest(REGISTER_REQUEST, registerRequest),
  ])
}

function* loginFlow() {
  while(true) {
    const { type, payload, meta, token } = yield take([LOGIN_REQUEST, LOGIN_BY_TOKEN_REQUEST])
    const task = (type === LOGIN_REQUEST)? yield fork(loginRequest, payload, meta) : yield fork(loginByTokenRequest, token)
    const action = yield take([LOGOUT_REQUEST, LOGIN_FAILED])
    if(action.type === LOGOUT_REQUEST) {
      localStorage.removeItem('token@my-portfolio-app')
      yield put(logout.success())
      yield cancel(task)
      yield put(push('/login'))
    }
  }
}

function* loginRequest(payload, meta) {
  try {
    const {user, token} = yield Api.login(payload)
    yield put(login.success({user}))
    localStorage.setItem('token@my-portfolio-app', token)
    yield put(push('/feeds'));
  } catch (e) {
    yield put(login.failed())
    yield handlerError(e, meta.reject)
  } finally {
    yield cancelled()
  }
}

function* loginByTokenRequest(token) {
  try {
    const { user } = yield Api.login_by_token(token)
    yield put(login.success({user}))
  } catch (e) {
    yield put(login.failed())
  } finally {
    yield cancelled()
  }
}

function* registerRequest({payload, meta}) {
  try {
    const {user, token} = yield Api.register({user: payload})
    yield put(push('/feeds'));
    yield put(register.success({user}))
  } catch (e) {
    yield put(register.failed())
    yield handlerError(e, meta.reject)
  }
}

function* handlerError(e, reject) {
  if(e instanceof HttpError) {
    const { errors } = yield e.json()
    reject(new SubmissionError(errors))
  } else {
    console.log(e);
    reject()
  }
}
