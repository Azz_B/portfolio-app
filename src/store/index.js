import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import rootReducer from './reducers'
import sagas from './sagas'

const __DEV__ = process.env.NODE_ENV === 'development'

export default function configureStore(history) {
  let store;
  const reducers = connectRouter(history)(rootReducer)
  const sagaMiddleware = createSagaMiddleware()
  const middlewares = [ routerMiddleware(history), sagaMiddleware ]
  let enhancer

  if(__DEV__) {
    middlewares.push(createLogger())
    enhancer = composeWithDevTools(applyMiddleware(...middlewares))
  } else {
    enhancer = applyMiddleware(...middlewares)
  }

  store = createStore(reducers, enhancer)

  if(__DEV__) {
    if(module.hot) {
      module.hot.accept('./reducers/index', () => {
        store.replaceReducer(require('./reducers/index'))
      })
    }
  }

  sagaMiddleware.run(sagas)
  
  return store
}
