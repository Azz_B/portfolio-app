import { createSelector } from 'reselect';


export const profileSelector = createSelector(
  select_user_by_username,
  profile => profile
)

export const profilePostsSelector = createSelector(
  select_user_posts_by_username,
  posts => posts
)

export const currentUserSelector = createSelector(
  select_current_user,
  user => user,
)

function select_user_by_username(state, {username}) {
  return state.users[username] || {}
}

function select_user_posts_by_username(state, {username}) {
  const keys = Object.keys(state.posts)
  const posts = []
  keys.forEach(key => {
    const post = state.posts[key]
    if(post.user.username === username) posts.push(post)
  });

  return posts
}

function select_current_user(state) {
  return state.users[state.auth.username]
}
