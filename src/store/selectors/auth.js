import { createSelector } from 'reselect';

export const loggedUserSelector = createSelector(
  select_logged_user,
  user => user
)

function select_logged_user(state) {
  const {id, fullname, photo} = state.users[state.auth.username] || {}
  return {...state.auth, id, fullname, photo}
}
