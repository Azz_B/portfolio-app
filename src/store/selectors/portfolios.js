import { createSelector } from 'reselect';


export const mainPortfolioSelector = createSelector(
  select_main_portfolio,
  portfolio => portfolio
)

export const portfolioSelector = createSelector(
  select_portolio_by_username,
  portfolio => portfolio
)

function select_main_portfolio(state) {
  return state.portfolios[state.main_portfolio] || {}
}

function select_portolio_by_username(state, {username}) {
  const keys = Object.keys(state.portfolios)
  for(let i = 0; i < keys.length; i++) {
    const portfolio = state.portfolios[keys[i]]
    if(portfolio.user.username === username) return portfolio 
  }

  return {}
}
