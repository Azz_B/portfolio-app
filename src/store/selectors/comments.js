import { createSelector } from 'reselect';


export const commentsSelector = createSelector(
  select_comments_by_post_id,
  post => post
)

function select_comments_by_post_id(state, {post_id}) {
  const comments = []
  Object.keys(state.comments).forEach(key => {
    const comment = state.comments[key]
    if(comment && comment['post_id'] === post_id) comments.push(comment)
  })

  return comments
}
