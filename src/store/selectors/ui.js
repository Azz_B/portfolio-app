import { createSelector } from 'reselect';

export const uiSelector = createSelector(
  select_ui,
  screen => screen
)

function select_ui(state, {screen}) {
  return state.ui[screen]
}
