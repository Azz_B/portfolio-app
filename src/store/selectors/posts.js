import { createSelector } from 'reselect';


export const postSelector = createSelector(
  select_post_by_id,
  post => post
)

export const feedsSelector = createSelector(
  select_feeds,
  posts => posts
)

export const selectedDraftSelector = createSelector(
  select_selected_draft,
  post => post
)

function select_post_by_id(state, {id}) {
  return state.posts[id] || {user: {}}
}

function select_feeds(state) {
  const posts = []
  Object.keys(state.feeds.posts).forEach(key => {
    const post = state.posts[key]
    if(post) posts.push(post)
  })

  return posts
}

function select_selected_draft(state) {
  if(!state.selected_draft) return {}
  return state.drafts[state.selected_draft]
}
