import { expect } from 'chai'
import { Reducer } from 'redux-testkit';
import { Data } from '../__utils__'
import reducer from '../../reducers/ui'
import {
  setSpinner,
} from '../../actions/ui'



describe('users | reducer', () => {
  const initialState = Data.initialState().ui

  it('should have an initial state', () => {
    const result = reducer(undefined, {type: ''})
    expect(result).to.deep.equal(initialState)
  })

  it('should handle SET_SPINNER action', () => {
    const action = setSpinner({screen: 'profile', element: 'userInfo', visible: true})
    const expected_result = {...initialState};
    expected_result.profile.spinners.userInfo = true

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

})
