import { Selector } from 'redux-testkit';
import { buildState } from '../__utils__'
import {
  uiSelector,
} from '../../selectors/ui'

describe('users | selector', () => {
  let state

  beforeAll(() => {
    state = buildState()
  })

  it("select ui profile state", () => {
    const result = Selector(uiSelector).execute(state, { screen: 'profile' })
    //console.log(result);
  })
})
