import { expect } from 'chai'
import { Reducer } from 'redux-testkit';
import { Data } from '../__utils__'
import reducer, { feeds as feedsReducer} from '../../reducers/posts'
import {
  get_post,
  get_feeds,
  get_user_posts,
} from '../../actions/posts'


describe('posts | reducer', () => {
  const initialState = {}

  it('should have an initial state', () => {
    const result = reducer(undefined, {type: ''})
    expect(result).to.deep.equal(initialState)
  })

  it('should handle GET_POST_SUCCESS action', () => {
    const data = Data.post()
    const action = get_post.success(data)
    const expected_result = {
      [data.post.id]: data.post,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

  it('should handle GET_FEEDS_SUCCESS action by posts reducer', () => {
    const data = Data.feeds()
    const action = get_feeds.success(data)
    const expected_result = {}

    data.posts.forEach(post => {
      expected_result[post.id] = post
    })

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

  it('should handle GET_FEEDS_SUCCESS action by feedsReducer', () => {
    const data = Data.feeds()
    const action = get_feeds.success(data)
    const expected_result = { posts: {} }

    data.posts.forEach(post => {
      expected_result.posts[post.id] = true
    })

    Reducer(feedsReducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

  it('should handle GET_USER_POSTS_SUCCESS action', () => {
    const data = Data.posts()
    const action = get_user_posts.success(data)
    const expected_result = {}

    data.posts.forEach(post => {
      expected_result[post.id] = post
    })

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

})
