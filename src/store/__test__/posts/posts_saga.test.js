import { expect } from 'chai'
import SagaTester from 'redux-saga-tester';
import { Data, httpError, resolveDelay } from '../__utils__'
import postsWatch from '../../sagas/posts'
import {
  GET_POST_SUCCESS,
  GET_POST_FAILED,
  get_post,
  GET_FEEDS_SUCCESS,
  GET_FEEDS_FAILED,
  get_feeds,
  GET_USER_POSTS_SUCCESS,
  GET_USER_POSTS_FAILED,
  get_user_posts,
  UPLOAD_DRAFT_VALUES_SUCCESS,
  UPLOAD_DRAFT_VALUES_FAILED,
  uploadDraftValues,
} from '../../actions/posts'

jest.mock('../../endpoints/ajax.js')


describe('posts | saga', () => {
  const ajax = require('../../endpoints/ajax.js').default
  let sagaTester;
  const meta = {reject: () => {}}

  beforeAll(() => {
    sagaTester = new SagaTester(Data.initialState())
    sagaTester.start(postsWatch)
  })

  beforeEach(() => {
    jest.resetAllMocks();
  })

  it('should Handle GET_POST_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.post()));
    sagaTester.dispatch(get_post.request({meta}))
    await sagaTester.waitFor(GET_POST_SUCCESS)
  })

  it('should Handle GET_POST_REQUEST action | failed', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(get_post.request({meta}))
    await sagaTester.waitFor(GET_POST_FAILED)
  })

  it('should Handle GET_FEEDS_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.feeds()));
    sagaTester.dispatch(get_feeds.request({meta}))
    await sagaTester.waitFor(GET_FEEDS_SUCCESS)
  })

  it('should Handle GET_FEEDS_REQUEST action | failed', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(get_feeds.request({meta}))
    await sagaTester.waitFor(GET_FEEDS_FAILED)
  })

  it('should Handle GET_USER_POSTS_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.posts()));
    sagaTester.dispatch(get_user_posts.request({meta}))
    await sagaTester.waitFor(GET_USER_POSTS_SUCCESS)
  })

  it('should Handle GET_USER_POSTS_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(get_user_posts.request({meta}))
    await sagaTester.waitFor(GET_USER_POSTS_FAILED)
  })

  it('should Handle UPLOAD_DRAFT_VALUES_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.post()));
    sagaTester.dispatch(uploadDraftValues.request({values: {title_draft: 'title'}, meta: {...meta, screen: 'draft', element: 'is_saving'}}))
    await sagaTester.waitFor(UPLOAD_DRAFT_VALUES_SUCCESS)
  })

  it('should Handle UPLOAD_DRAFT_VALUES_REQUEST action | failed', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(uploadDraftValues.request({values: {title_draft: 'title'}, meta: {...meta, screen: 'draft', element: 'is_saving'}}))
    await sagaTester.waitFor(UPLOAD_DRAFT_VALUES_FAILED)
  })
})
