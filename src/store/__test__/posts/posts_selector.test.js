import { Selector } from 'redux-testkit';
import { buildState } from '../__utils__'
import {
  postSelector,
  feedsSelector,
  selectedDraftSelector,
} from '../../selectors/posts'


describe('posts | selector', () => {
  let state

  beforeAll(() => {
    state = buildState()
  })

  it('select post by id', () => {
    const result = Selector(postSelector).execute(state, { id: 'some_post_id-1' })
    // console.log(result);
  })

  it('select feeds', () => {
    const result = Selector(feedsSelector).execute(state)
    //console.log(result);
  })

  it('select selected draft', () => {
    const result = Selector(selectedDraftSelector).execute(state)
  })

})
