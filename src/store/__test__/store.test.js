import { expect } from 'chai'
import { createMemoryHistory } from 'history';
import configureStore from '../'
import { Data } from './__utils__'

describe('store', () => {
  let store;

  beforeEach(() => {
    store = configureStore(createMemoryHistory())
  })

  it('should have an initial state', () => {
    const initialState = Data.initialState()

    const keys = Object.keys(initialState)
    const state = store.getState()

    keys.forEach( key => expect(state[key]).to.deep.equal(initialState[key]))
  })
})
