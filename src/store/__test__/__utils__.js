import HttpError from '../endpoints/HttpError'

const initialState = {
  auth: {
    username: null,
  },
  main_portfolio: null,
  portfolios: {},
  users: {},
  posts: {},
  selected_draft: null,
  drafts: {},
  comments: {},
  feeds: {
    posts: {},
  },
  ui: {
    auth: {
      spinners: {
        loading: false,
      },
    },

    profile: {
      spinners: {
        userInfo: false,
        posts: false,
        drafts: false,
      },
      config: {
        posts_limit: 5,
        posts_offset: 0,
        posts_hasMore: true,
        drafts_limit: 5,
        drafts_offset: 0,
        drafts_hasMore: true,
      },
    },

    draft: {
      spinners: {
        is_saving: false,
      },
    },

    feeds: {
      spinners: {
        loading: false,
      },
      config: {
        limit: 10,
        offset: 0,
        hasMore: true,
      },
    },

    post: {
      spinners: {
        loading: false,
        comments_loading: false,
      },
    },

  },
};

const user = {id: 'some_user_id', username: 'some_username', fullname: 'some_fullname', bio: 'some_bio', photo: 'some_photo'}
const portfolio = {id: 'some_portolio_id',intro: 'some_intro', status: 'some_status', user: {id: 'some_user_id', username: 'some_username'}}
const post = {id: 'some_post_id', title: 'some_title', subtitle: 'some_subtitle', image: 'some_image', content: 'some_content', user: {id: 'some_user_id', username: 'some_username', photo: 'user_photo'}, likes_count: 5, comments_count: 3, you_liked: false, created_at: 'created_at'}
const posts = [1, 2].map(i => ({...post, id: `${post.id}-${i}`}))
const comment = {id: 'some_comment_id', content: 'some_content', user: {id: 'some_user_id', username: 'some_username', photo: 'user_photo'}, replied_to: null, likes_count: 1, you_liked: false, created_at: 'created_at', post_id: 'some_post_id'}
const comments = [1, 2].map(i => ({...comment, id: `${comment.id}-${i}`}))

export const Data = {
  initialState: () => ({...initialState}),
  portfolio: (data) => ({portfolio: {...portfolio, ...data}}),
  auth: () => ({token: 'some_token', user: {...user}}),
  user: assign('user', user),
  post: assign('post', post),
  posts: assign('posts', posts, 'array'),
  feeds: assign('posts', posts, 'array'),
  comments: assign('comments', comments, 'array'),
  error: () => ({reason: 'some_reason', errors: {}}),
}

export const httpError = (json) => (
  new HttpError({
    json: () => Promise.resolve({ ...json }),
  })
);

const delay = (cb, params = {}, time) => {
  setTimeout(() => cb(params), time);
}

export const promiseDelay = (params, resolveIt = true, time = 500) => (
  new Promise((resolve, reject) => {
    if(resolveIt)
      delay(resolve, params, time);
    else
      delay(reject, params, time);
  })
);

export const resolveDelay = (params, time) => promiseDelay(params, true, time);
export const rejectDelay = (params, time) => promiseDelay(params, false, time);

function assign(key, base, type) {
  return (added = {}) => {
    const data = (type === 'array')? base : { ...base, ...added }

    return key? {[key]: data } : data
  }
}


export function buildState() {
  const portfolios = {};
  [1, 2].map(i => alter(portfolio, i)).forEach(portfolio => portfolios[portfolio.id] = portfolio)

  const users = {};
  [1, 2].map(i => alter(user, i)).forEach(user => users[user.username] = user)

  const posts = {};
  [1, 2].map(i => alter(post, i)).forEach(post => posts[post.id] = post)

  const comments = {};
  [1, 2].map(i => alter(comment, i)).forEach(comment => comments[comment.id] = comment)

  const feeds = { posts: {} }
  Object.keys(posts).forEach(key => feeds.posts[key] = true)

  const username = Object.keys(users)[0]

  return {
    auth: { username },
    main_portfolio: 'some_portolio_id-1',
    portfolios,
    users,
    posts,
    comments,
    feeds,
    ui: {...initialState.ui },
  }
}

function alter(value, i) {
  if(Array.isArray(value)) {
    return value.map( v => (typeof v === "string" ? `${v}-${i}` : v) )
  }

  if(typeof value === 'object' && value !== null) {
    return Object.keys(value).reduce((prev, key) => {
      prev[key] = alter(value[key], i)
      return prev
    }, {})
  }

  return (typeof value === "string")? `${value}-${i}` : value
}
