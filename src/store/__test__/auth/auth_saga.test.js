import { expect } from 'chai'
import SagaTester from 'redux-saga-tester';
import { Data, httpError, resolveDelay } from '../__utils__'
import authWatch from '../../sagas/auth'
import {
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  login,
  LOGOUT_SUCCESS,
  logout,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  register,
} from '../../actions/auth'

jest.mock('../../endpoints/ajax.js')


describe('auth | saga', () => {
  const ajax = require('../../endpoints/ajax.js').default
  let sagaTester;
  const meta = {reject: () => {}}

  beforeAll(() => {
    sagaTester = new SagaTester(Data.initialState())
    sagaTester.start(authWatch)
  })

  beforeEach(() => {
    jest.resetAllMocks();
  })

  it('should Handle LOGIN_REQUEST action | flow 1', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.auth()));

    sagaTester.dispatch(login.request({meta}))
    await sagaTester.waitFor(LOGIN_SUCCESS)

    sagaTester.dispatch(login.request({meta}))
    const action = await Promise.race([
      resolveDelay('ok'),
      sagaTester.waitFor(LOGIN_SUCCESS, true)
    ]);

    expect(action.type).to.not.equal(LOGIN_SUCCESS)

    sagaTester.dispatch(logout.request())

    await sagaTester.waitFor(LOGOUT_SUCCESS)
  })

  it('should Handle LOGIN_REQUEST action | flow 2', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(login.request({meta}))
    await sagaTester.waitFor(LOGIN_FAILED)
  })

  it('should handle REGISTER_REQUEST action | flow 1', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.auth()));
    sagaTester.dispatch(register.request({meta}))
    await sagaTester.waitFor(REGISTER_SUCCESS)
  })

  it('should handle REGISTER_REQUEST action | flow 2', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(register.request({meta}))
    await sagaTester.waitFor(REGISTER_FAILED)
  })
})
