import { expect } from 'chai'
import { Reducer } from 'redux-testkit';
import { Data } from '../__utils__'
import reducer from '../../reducers/auth'
import {
  login,
  register,
} from '../../actions/auth'


describe('auth | reducer', () => {
  const initialState = {
    username: null,
  }

  it('should have an initial state', () => {
    const result = reducer(undefined, {type: ''})
    expect(result).to.deep.equal(initialState)
  })

  it('should handle LOGIN_SUCCESS action', () => {
    const data = Data.auth()
    const action = login.success(data)
    const expected_result = {
      username: data.user.username,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

  it('should handle REGISTER_SUCCESS action', () => {
    const data = Data.auth()
    const action = register.success(data)
    const expected_result = {
      username: data.user.username,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })
})
