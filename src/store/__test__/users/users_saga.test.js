import { expect } from 'chai'
import SagaTester from 'redux-saga-tester';
import { Data, httpError, resolveDelay } from '../__utils__'
import usersWatch from '../../sagas/users'
import {
  GET_USER_SUCCESS,
  GET_USER_FAILED,
  get_user,
} from '../../actions/users'
import {
  SET_SPINNER
} from '../../actions/ui'

jest.mock('../../endpoints/ajax.js')


describe('users | saga', () => {
  const ajax = require('../../endpoints/ajax.js').default
  let sagaTester;
  const meta = {reject: () => {}, screen: 'profile', element: 'userInfo'}

  beforeAll(() => {
    sagaTester = new SagaTester(Data.initialState())
    sagaTester.start(usersWatch)
  })

  beforeEach(() => {
    jest.resetAllMocks();
  })

  it('should Handle GET_USER_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.user()));
    sagaTester.dispatch(get_user.request({meta}))
    await sagaTester.waitFor(SET_SPINNER)
    await sagaTester.waitFor(GET_USER_SUCCESS)
    await sagaTester.waitFor(SET_SPINNER)
  })

  it('should Handle GET_USER_REQUEST action | failed', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(get_user.request({meta}))
    await sagaTester.waitFor(GET_USER_FAILED)
  })

})
