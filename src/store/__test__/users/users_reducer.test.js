import { expect } from 'chai'
import { Reducer } from 'redux-testkit';
import { Data } from '../__utils__'
import reducer from '../../reducers/users'
import {
  login,
  register,
} from '../../actions/auth'
import {
  get_user,
} from '../../actions/users'



describe('users | reducer', () => {
  const initialState = {}

  it('should have an initial state', () => {
    const result = reducer(undefined, {type: ''})
    expect(result).to.deep.equal(initialState)
  })

  it('should handle LOGIN_SUCCESS action', () => {
    const data = Data.auth()
    const action = login.success(data)
    const expected_result = {
      [data.user.username]: data.user,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

  it('should handle REGISTER_SUCCESS action', () => {
    const data = Data.auth()
    const action = register.success(data)
    const expected_result = {
      [data.user.username]: data.user,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

  it('should hande GET_USER_SUCCESS action', () => {
    const data = Data.user()
    const action = get_user.success(data)
    const expected_result = {
      [data.user.username]: data.user,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

})
