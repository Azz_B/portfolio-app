import { Selector } from 'redux-testkit';
import { buildState } from '../__utils__'
import {
  profileSelector,
  profilePostsSelector,
} from '../../selectors/users'

describe('users | selector', () => {
  let state

  beforeAll(() => {
    state = buildState()
  })

  it('select user profile', () => {
    const expected_result = {
      id: 'some_user_id-1',
      username: 'some_username-1',
      fullname: 'some_fullname-1',
      bio: 'some_bio-1',
      photo: 'some_photo-1',
    }

    Selector(profileSelector)
      .expect(state, {username: 'some_username-1'})
      .toReturn(expected_result)
  })

  it("select user profile posts", () => {
    const result = Selector(profilePostsSelector).execute(state, { username: 'some_username-1'})
  })
})
