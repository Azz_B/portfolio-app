import { expect } from 'chai'
import SagaTester from 'redux-saga-tester';
import { Data } from '../__utils__'
import portfolioWatch from '../../sagas/portfolio'
import {
  MAIN_PORTFOLIO_SUCCESS,
  mainPortfolio,
} from '../../actions/portfolio'

jest.mock('../../endpoints/ajax.js')


describe('portfolio | saga', () => {
  const ajax = require('../../endpoints/ajax.js').default
  let sagaTester;

  beforeAll(() => {
    sagaTester = new SagaTester(Data.initialState())
    sagaTester.start(portfolioWatch)
  })

  beforeEach(() => {
    jest.resetAllMocks();
  })

  it('should Handle MAIN_PORTFOLIO_REQUEST action', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.portfolio()));
    sagaTester.dispatch(mainPortfolio.request())
    await sagaTester.waitFor(MAIN_PORTFOLIO_SUCCESS)
  })
})
