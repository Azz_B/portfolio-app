import { expect } from 'chai'
import { Reducer } from 'redux-testkit';
import reducer from '../../reducers/portfolios'
import {
  mainPortfolio,
} from '../../actions/portfolio'


describe('portfolios | reducer', () => {
  const initialState = {}

  it('should have an initial state', () => {
    const result = reducer(undefined, {type: ''})
    expect(result).to.deep.equal(initialState)
  })

  it('should handle MAIN_PORTFOLIO_SUCCESS action', () => {
    const data = {id: 'some_portolio_id',intro: 'some_intro', status: 'some_status', user: {id: 'some_user_id', username: 'some_username'}}
    const action = mainPortfolio.success({portfolio: data})
    const expected_result = {
      'some_portolio_id': data,
    };

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })
})
