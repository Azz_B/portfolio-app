import { Selector } from 'redux-testkit';
import { buildState } from '../__utils__'
import {
  mainPortfolioSelector,
  portfolioSelector,
} from '../../selectors/portfolios'


describe('portfolios | selector', () => {
  let state

  beforeAll(() => {
    state = buildState()
  })

  it('select main portfolio', () => {
    const result = Selector(mainPortfolioSelector).execute(state)
    //console.log(result);
  })

  it('select user portfolio by username', () => {
    const result = Selector(portfolioSelector).execute(state, {username: 'some_username-2'})
    //console.log(result);
  })

})
