import { expect } from 'chai'
import { Reducer } from 'redux-testkit';
import { Data } from '../__utils__'
import reducer from '../../reducers/comments'
import {
  get_post_comments,
} from '../../actions/comments'


describe('comments | reducer', () => {
  const initialState = {}

  it('should have an initial state', () => {
    const result = reducer(undefined, {type: ''})
    expect(result).to.deep.equal(initialState)
  })

  it('should handle GET_POST_COMMENTS_SUCCESS action', () => {
    const data = Data.comments()
    const action = get_post_comments.success(data)
    const expected_result = {}

    data.comments.forEach(comment => expected_result[comment.id] = comment)

    Reducer(reducer)
      .withState(initialState)
      .expect(action)
      .toReturnState(expected_result)
  })

})
