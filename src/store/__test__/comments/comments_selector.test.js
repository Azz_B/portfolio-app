import { Selector } from 'redux-testkit';
import { buildState } from '../__utils__'
import {
  commentsSelector,
} from '../../selectors/comments'


describe('comments | selector', () => {
  let state

  beforeAll(() => {
    state = buildState()
  })

  it('select comments by post_id', () => {
    const result = Selector(commentsSelector).execute(state, { post_id: 'some_post_id-1' })
    //console.log(result);
  })

})
