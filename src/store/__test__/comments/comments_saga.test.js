import { expect } from 'chai'
import SagaTester from 'redux-saga-tester';
import { Data, httpError, resolveDelay } from '../__utils__'
import commentsWatch from '../../sagas/comments'
import {
  GET_POST_COMMENTS_SUCCESS,
  GET_POST_COMMENTS_FAILED,
  get_post_comments,
} from '../../actions/comments'

jest.mock('../../endpoints/ajax.js')


describe('comments | saga', () => {
  const ajax = require('../../endpoints/ajax.js').default
  let sagaTester;
  const meta = {reject: () => {}}

  beforeAll(() => {
    sagaTester = new SagaTester(Data.initialState())
    sagaTester.start(commentsWatch)
  })

  beforeEach(() => {
    jest.resetAllMocks();
  })

  it('should Handle GET_POST_COMMENTS_REQUEST action | success', async () => {
    ajax.mockReturnValue(Promise.resolve(Data.comments()));
    sagaTester.dispatch(get_post_comments.request({post_id: 'id', meta}))
    await sagaTester.waitFor(GET_POST_COMMENTS_SUCCESS)
  })

  it('should Handle GET_POST_COMMENTS_REQUEST action | failed', async () => {
    ajax.mockReturnValue(Promise.reject(httpError(Data.error())));
    sagaTester.dispatch(get_post_comments.request({meta}))
    await sagaTester.waitFor(GET_POST_COMMENTS_FAILED)
  })


})
