import HttpError from './HttpError';

export default function ajax(url, { method= 'GET', headers = {}, body={}} = {}) {
  method = method.toLowerCase();

  let defaultHeaders = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  };
  headers = { ...defaultHeaders, ...headers };

  const options = {
    method,
    headers
  };

  (method !== 'get' && method !== 'head')? options.body = JSON.stringify(body) : void 0;

  return fetch(url, options).then(checkStatus).then(getJSON);
}

export function checkStatus(response) {
  if(response.status >= 200 && response.status < 300 ) {
    return Promise.resolve(response);
  }

  return Promise.reject(new HttpError(response));
  //return Promise.reject(response)
}

export function getJSON(response) {
  return response.json();
}
