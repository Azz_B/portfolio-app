import ajax from '../ajax';
import { BASE_URL } from '../config'

const URL = `${BASE_URL}/api_v1/portfolios`


function mainPortfolio() {
  return ajax(`${URL}/main`)
}


export default {
  mainPortfolio,
}
