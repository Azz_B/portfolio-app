import ajax from '../ajax';
import { BASE_URL } from '../config'
import { get_user_posts } from './users'

const URL = `${BASE_URL}/api_v1/posts`


function get_post(id) {
  return ajax(`${URL}/${id}`)
}

function get_feeds(limit, offset, token) {
  const headers = token? {'Authorization': `Bearer ${token}`} : {}

  return ajax(`${URL}?limit=${limit}&offset=${offset}`, {
    headers,
  })
}

export function get_post_comments(post_id) {
  return ajax(`${URL}/${post_id}/comments`)
}

export function create_post(values, token) {
  return ajax(URL, {
    method: 'post',
    body: values,
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
}

export function update_post(id, values, token) {
  console.log('update_post');
  return ajax(`${URL}/${id}`, {
    method: 'put',
    body: values,
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
}

export function publish_post(id, token) {
  return ajax(`${URL}/${id}/publish`, {
    method: 'post',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
}

export function create_comment(id, body, token) {
  return ajax(`${URL}/${id}/comments`, {
    method: 'post',
    body,
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
}

export default {
  get_post,
  get_feeds,
  get_user_posts,
  create_post,
  update_post,
  publish_post,
}
