import ajax from '../ajax';
import { BASE_URL } from '../config'
import {
  get_post_comments,
  create_comment,
} from './posts'

//const URL = `${BASE_URL}/api_v1/comments`

export default {
  get_post_comments,
  create_comment,
}
