import ajax from '../ajax';
import { BASE_URL } from '../config'

const URL = `${BASE_URL}/auth`


export function login(body) {
  return ajax(`${URL}/login`, {
    method: 'POST',
    body,
  })
}

export function login_by_token(token) {
  return ajax(`${URL}/login_by_token`, {
    method: 'get',
    headers: {
      'x-access-token': token,
    },
  })
}

export function register(body) {
  return ajax(`${URL}/register`, {
    method: 'POST',
    body
  })
}


export default {
  login,
  login_by_token,
  register,
}
