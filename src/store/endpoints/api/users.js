import ajax, { checkStatus, getJSON } from '../ajax';
import { BASE_URL } from '../config'

const URL = `${BASE_URL}/api_v1/users`


function get_user(username) {
  return ajax(`${URL}/${username}`)
}

export function get_user_posts(username) {
  return ajax(`${URL}/${username}/posts`)
}

function update_user(body, token) {
  return ajax(URL, {
    method: 'put',
    body,
    headers: {
      'Authorization': `Bearer ${token}`,
    },
  })
}

function upload_user_photo(body, token) {
  return fetch(`${URL}/photo`, {
    method: 'POST',
    headers: {
      'Authorization': `Bearer ${token}`,
    },
    body: body,
  }).then(checkStatus).then(getJSON)
}


export default {
  get_user,
  update_user,
  upload_user_photo,
}
