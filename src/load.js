import { injectGlobal } from 'styled-components'
import 'semantic-ui-css/semantic.min.css';

window.LOG = (message, ...args) => {
  if( process.env.NODE_ENV !== 'development') return;

  console.log('-----------'+ message +'--------');
  console.log(...args);
  console.log('------------------------------------');
}


injectGlobal`
  * {
    margin: 0;
    box-sizing: border-box;
  }

  html, body {
    font-family: -webkit-system, BlinkMacSystemFont, sans-serif;
    font-size: 16px;
    line-height: 20px;
    height: 100%;
  }

  #root {
    height: 100%;
  }
`
